// Copyright © 2019 Phil Booth
//
// This file is part of pbvi.
//
// pbvi is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// pbvi is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// pbvi. If not, see <https://www.gnu.org/licenses/>.

use std::{
    error::Error,
    io::{stdout, Stdout, Write},
    sync::Arc,
    thread,
};

use parking_lot::Mutex;
use signal_hook::iterator::Signals;
use termion::{
    cursor,
    raw::{IntoRawMode, RawTerminal},
    terminal_size,
};

pub trait Window {
    fn new() -> Arc<Mutex<Self>>;
    fn draw(&mut self, row: usize, column: usize, text: &str) -> Result<(), Box<Error>>;
    fn get_size(&self) -> (usize, usize);
}

pub struct RealWindow {
    terminal: RawTerminal<Stdout>,
    width: usize,
    height: usize,
}

impl Window for RealWindow {
    fn new() -> Arc<Mutex<Self>> {
        let mut window = Self {
            terminal: stdout().into_raw_mode().unwrap(),
            width: 0,
            height: 0,
        };
        window.set_size();
        let mutex = Arc::new(Mutex::new(window));

        {
            let mutex = mutex.clone();
            let signals = Signals::new(&[signal_hook::SIGWINCH]).unwrap();
            thread::spawn(move || {
                for _ in signals.pending() {
                    let mut w = mutex.lock();
                    (*w).set_size();
                }
            });
        }

        mutex
    }

    fn draw(&mut self, row: usize, column: usize, text: &str) -> Result<(), Box<Error>> {
        write!(
            &mut self.terminal,
            "{}{}",
            cursor::Goto(column as u16 + 1, row as u16 + 1),
            text
        )?;
        self.terminal.flush()?;
        Ok(())
    }

    fn get_size(&self) -> (usize, usize) {
        (self.width, self.height)
    }
}

impl RealWindow {
    pub fn set_size(&mut self) {
        let size = terminal_size().unwrap();
        self.width = size.0 as usize;
        self.height = size.1 as usize;
    }
}

pub struct MockWindow {}

impl Window for MockWindow {
    fn new() -> Arc<Mutex<Self>> {
        Arc::new(Mutex::new(Self {}))
    }

    fn draw(&mut self, _row: usize, _column: usize, _text: &str) -> Result<(), Box<Error>> {
        Ok(())
    }

    fn get_size(&self) -> (usize, usize) {
        (2, 3)
    }
}
