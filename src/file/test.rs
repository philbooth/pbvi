// Copyright © 2018, 2019 Phil Booth
//
// This file is part of pbvi.
//
// pbvi is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// pbvi is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// pbvi. If not, see <https://www.gnu.org/licenses/>.

use super::*;

#[test]
fn non_existent_file() {
    let file = File::new("fixtures/does-not-exist.txt");

    assert_eq!(file.path().as_os_str(), "fixtures/does-not-exist.txt");
    assert_eq!(file.exists().unwrap(), false);

    assert!(file.is_directory().is_err());
    match file.is_directory() {
        Err(error) => assert_eq!(error.kind(), io::ErrorKind::NotFound),
        _ => {}
    }

    assert!(file.last_modified_at().is_err());
    match file.last_modified_at() {
        Err(error) => assert_eq!(error.kind(), io::ErrorKind::NotFound),
        _ => {}
    }

    assert!(file.read().is_err());
    match file.read() {
        Err(error) => assert_eq!(error.kind(), io::ErrorKind::NotFound),
        _ => {}
    }
}

#[test]
fn utf16_file() {
    let file = File::new("fixtures/utf16.txt");

    assert_eq!(file.path().as_os_str(), "fixtures/utf16.txt");
    assert_eq!(file.exists().unwrap(), true);
    assert_eq!(file.is_directory().unwrap(), false);
    assert!(file.last_modified_at().is_ok());

    assert!(file.read().is_err());
    match file.read() {
        Err(error) => {
            assert_eq!(error.kind(), io::ErrorKind::InvalidData);
            assert_eq!(error.description(), "UTF-16 (little-endian) BOM detected");
        }
        _ => {}
    }
}

#[test]
fn utf32_file() {
    let file = File::new("fixtures/utf32.txt");

    assert_eq!(file.path().as_os_str(), "fixtures/utf32.txt");
    assert_eq!(file.exists().unwrap(), true);
    assert_eq!(file.is_directory().unwrap(), false);
    assert!(file.last_modified_at().is_ok());

    assert!(file.read().is_err());
    match file.read() {
        Err(error) => {
            assert_eq!(error.kind(), io::ErrorKind::InvalidData);
            assert_eq!(error.description(), "UTF-32 (little-endian) BOM detected");
        }
        _ => {}
    }
}

#[test]
fn ascii_file() {
    let file = File::new("fixtures/ascii.txt");

    assert_eq!(file.path().as_os_str(), "fixtures/ascii.txt");
    assert_eq!(file.exists().unwrap(), true);
    assert_eq!(file.is_directory().unwrap(), false);
    assert!(file.last_modified_at().is_ok());
    assert_eq!(
        file.read().unwrap(),
        include_str!("../../fixtures/ascii.txt")
    );
}

#[test]
fn utf8_file() {
    let file = File::new("fixtures/utf8.txt");

    assert_eq!(file.path().as_os_str(), "fixtures/utf8.txt");
    assert_eq!(file.exists().unwrap(), true);
    assert_eq!(file.is_directory().unwrap(), false);
    assert!(file.last_modified_at().is_ok());
    assert_eq!(
        file.read().unwrap(),
        include_str!("../../fixtures/utf8.txt")
    );
}
