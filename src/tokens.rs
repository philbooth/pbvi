// Copyright © 2018, 2019 Phil Booth
//
// This file is part of pbvi.
//
// pbvi is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// pbvi is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// pbvi. If not, see <https://www.gnu.org/licenses/>.

mod arena;
mod children;
mod error;
mod iterator;
mod parent;
pub mod parse;
mod siblings;
#[cfg(test)]
mod test;
mod value;

use std::{
    fmt::{self, Display, Formatter},
    sync::Arc,
};

use lazy_static::lazy_static;
use parking_lot::{RwLock, RwLockReadGuard};
use regex::Regex;

pub use self::{arena::TokenArena, iterator::TokenIterator};
use self::{
    children::Children,
    error::{TokenError, TokenResult},
    parent::Parent,
    siblings::Siblings,
    value::Value,
};

lazy_static! {
    static ref WORD_PATTERN: Regex = Regex::new(r"^\w+$").unwrap();
    static ref SPACE_PATTERN: Regex = Regex::new(r"^\s+$").unwrap();
}

#[derive(Debug)]
pub enum Token {
    Null {
        arena: Arc<RwLock<TokenArena>>,
    },
    Grapheme {
        arena: Arc<RwLock<TokenArena>>,
        value: Value,
        siblings: Siblings,
        parent: Parent,
    },
    Word {
        arena: Arc<RwLock<TokenArena>>,
        siblings: Siblings,
        children: Children,
        parent: Parent,
    },
    Line {
        arena: Arc<RwLock<TokenArena>>,
        siblings: Siblings,
        children: Children,
        parent: Parent,
    },
    Paragraph {
        arena: Arc<RwLock<TokenArena>>,
        siblings: Siblings,
        children: Children,
    },
}

pub type TokenId = usize;

macro_rules! create_token {
    ($arena:expr, $id:ident, $type:ident { $($key:ident: $value:expr,)+ }) => {
        Token::$type {
            arena: $arena,
            siblings: Siblings::new($arena, $id, TokenType::$type),
            $($key: $value,)+
        }
    }
}

impl Token {
    fn new_grapheme(arena: Arc<RwLock<TokenArena>>, id: TokenId, value: &str) -> Self {
        create_token!(
            arena.clone(),
            id,
            Grapheme {
                value: Value::new(value),
                parent: Parent::new(arena),
            }
        )
    }

    fn new_word(arena: Arc<RwLock<TokenArena>>, id: TokenId, graphemes: &[TokenId]) -> Self {
        let token = create_token!(
            arena.clone(),
            id,
            Word {
                children: Children::new(arena.clone(), graphemes, TokenType::Word),
                parent: Parent::new(arena.clone()),
            }
        );
        link_children(arena, id, graphemes);
        token
    }

    fn new_line(arena: Arc<RwLock<TokenArena>>, id: TokenId, words: &[TokenId]) -> Self {
        let token = create_token!(
            arena.clone(),
            id,
            Line {
                children: Children::new(arena.clone(), words, TokenType::Line),
                parent: Parent::new(arena.clone()),
            }
        );
        link_children(arena, id, words);
        token
    }

    fn new_paragraph(arena: Arc<RwLock<TokenArena>>, id: TokenId, lines: &[TokenId]) -> Self {
        let token = create_token!(
            arena.clone(),
            id,
            Paragraph {
                children: Children::new(arena.clone(), lines, TokenType::Paragraph),
            }
        );
        link_children(arena, id, lines);
        token
    }

    pub fn len(&self) -> usize {
        if self.is_grapheme() {
            1
        } else {
            self.children_or(0, &|children| children.len())
        }
    }

    pub fn is_grapheme(&self) -> bool {
        match *self {
            Token::Grapheme { .. } => true,
            _ => false,
        }
    }

    pub fn is_newline_grapheme(&self) -> bool {
        self.value_or(false, &|value| match value.as_ref() {
            "\n" => true,
            "\r" => true,
            "\r\n" => true,
            _ => false,
        })
    }

    pub fn is_newline_word(&self) -> bool {
        match *self {
            Token::Word { ref children, .. } => {
                if let Some(first_id) = children.first() {
                    self.arena()
                        .read(first_id, &|first| first.is_newline_grapheme())
                } else {
                    false
                }
            }
            _ => false,
        }
    }

    fn arena(&self) -> RwLockReadGuard<TokenArena> {
        match *self {
            Token::Null { ref arena, .. } => arena.read(),
            Token::Grapheme { ref arena, .. } => arena.read(),
            Token::Word { ref arena, .. } => arena.read(),
            Token::Line { ref arena, .. } => arena.read(),
            Token::Paragraph { ref arena, .. } => arena.read(),
        }
    }

    pub fn is_newline_line(&self) -> bool {
        match *self {
            Token::Line { ref children, .. } => {
                if let Some(first_id) = children.first() {
                    self.arena()
                        .read(first_id, &|first| first.is_newline_word())
                } else {
                    false
                }
            }
            _ => false,
        }
    }

    pub fn is_word_grapheme(&self) -> bool {
        self.value_or(false, &|value| WORD_PATTERN.is_match(value.as_ref()))
    }

    pub fn is_word(&self) -> bool {
        match *self {
            Token::Word { ref children, .. } => {
                if let Some(first_id) = children.first() {
                    self.arena()
                        .read(first_id, &|first| first.is_word_grapheme())
                } else {
                    false
                }
            }
            _ => false,
        }
    }

    pub fn is_space_grapheme(&self) -> bool {
        self.value_or(false, &|value| SPACE_PATTERN.is_match(value.as_ref()))
    }

    pub fn is_space(&self) -> bool {
        match *self {
            Token::Word { ref children, .. } => {
                if let Some(first_id) = children.first() {
                    self.arena()
                        .read(first_id, &|first| first.is_space_grapheme())
                } else {
                    false
                }
            }
            _ => false,
        }
    }

    pub fn is_first_child(&self) -> bool {
        if let Some(parent_id) = self.parent() {
            let this_id = self.siblings(&|siblings| siblings.this());
            let first_id = self
                .arena()
                .read(parent_id, &|parent| parent.first_child().unwrap());
            this_id == first_id
        } else {
            false
        }
    }

    pub fn iter(&self, first: Option<TokenId>, last: Option<TokenId>) -> TokenIterator {
        self.siblings_or(TokenIterator::null(self.clone_arena()), &|siblings| {
            siblings.iter(first, last)
        })
    }

    pub fn parent(&self) -> Option<TokenId> {
        self.parent_or(None, &|parent| parent.get())
    }

    fn set_parent(&mut self, parent_id: TokenId) {
        self.parent_mut(&|parent| parent.set(parent_id));
    }

    pub fn terminates(&self) -> TokenType {
        self.siblings(&|siblings| siblings.terminates())
    }

    pub fn set_terminates(&mut self, terminates: TokenType) {
        self.siblings_mut(&|siblings| siblings.set_terminates(terminates));
        self.children_mut(&|children| children.set_terminates(terminates));
    }

    pub fn clear_terminates(&mut self) {
        self.siblings_mut(&|siblings| siblings.clear_terminates());
    }

    pub fn previous(&self) -> Option<TokenId> {
        self.siblings(&|siblings| siblings.previous())
    }

    pub fn set_previous(&mut self, previous: TokenId) {
        self.siblings_mut(&|siblings| siblings.set_previous(previous));
    }

    pub fn set_previous_and_cascade(&mut self, previous_id: TokenId) {
        self.siblings_mut(&|siblings| siblings.set_previous_and_cascade(previous_id));
        self.children_mut(&|children| {
            children.set_child_previous_and_cascade(previous_id);
        });
    }

    pub fn set_child_previous_and_cascade(&mut self, previous_id: TokenId) {
        self.children_mut(&|children| {
            children.set_previous_and_cascade(previous_id);
        });
    }

    pub fn clear_previous(&mut self) {
        self.siblings_mut(&|siblings| siblings.clear_previous());
    }

    pub fn preceding_siblings(&self) -> TokenIterator {
        if let Some(previous_id) = self.previous() {
            self.arena().read(previous_id, &|previous| {
                previous.iter(None, self.previous())
            })
        } else {
            TokenIterator::null(self.clone_arena())
        }
    }

    fn clone_arena(&self) -> Arc<RwLock<TokenArena>> {
        match *self {
            Token::Null { ref arena, .. } => arena.clone(),
            Token::Grapheme { ref arena, .. } => arena.clone(),
            Token::Word { ref arena, .. } => arena.clone(),
            Token::Line { ref arena, .. } => arena.clone(),
            Token::Paragraph { ref arena, .. } => arena.clone(),
        }
    }

    pub fn next(&self) -> Option<TokenId> {
        self.siblings(&|siblings| siblings.next())
    }

    pub fn set_next(&mut self, next_id: TokenId) {
        self.siblings_mut(&|siblings| siblings.set_next(next_id));
    }

    pub fn set_next_and_cascade(&mut self, next_id: TokenId) {
        self.siblings_mut(&|siblings| siblings.set_next_and_cascade(next_id));
        self.children_mut(&|children| {
            children.set_child_next_and_cascade(next_id);
        });
    }

    pub fn set_child_next_and_cascade(&mut self, next_id: TokenId) {
        self.children_mut(&|children| {
            children.set_next_and_cascade(next_id);
        });
    }

    pub fn clear_next(&mut self) {
        self.siblings_mut(&|siblings| siblings.clear_next());
    }

    pub fn following_siblings(&self) -> TokenIterator {
        if let Some(next_id) = self.next() {
            self.arena()
                .read(next_id, &|next| next.iter(self.next(), None))
        } else {
            TokenIterator::null(self.clone_arena())
        }
    }

    pub fn first_child(&self) -> Option<TokenId> {
        self.children_or(None, &|children| children.first())
    }

    pub fn last_child(&self) -> Option<TokenId> {
        self.children_or(None, &|children| children.last())
    }

    pub fn value<'v>(&'v self) -> Option<&'v str> {
        self.value_or(None, &|value| Some(value.as_ref()))
    }

    pub fn delete(&mut self) {
        let previous_id = self.previous();
        let next_id = self.next();
        let deleted_id = self.delete_self_and_descendants();
        self.parent_mut(&|parent| parent.delete(deleted_id, previous_id, next_id));
    }

    fn delete_self_and_descendants(&mut self) -> TokenId {
        self.children_mut(&|children| children.delete_all());
        self.siblings_mut(&|siblings| siblings.delete())
    }

    fn delete_child(
        &mut self,
        child_id: TokenId,
        previous_id: Option<TokenId>,
        next_id: Option<TokenId>,
    ) {
        self.children_mut(&|children| children.delete(child_id, previous_id, next_id));
    }

    pub fn reinstate(&mut self) {
        let previous_id = self.previous();
        let next_id = self.next();
        let reinstated_id = self.reinstate_self_and_descendants();
        self.parent_mut(&|parent| parent.reinstate(reinstated_id, previous_id, next_id));
    }

    fn reinstate_self_and_descendants(&mut self) -> TokenId {
        self.children_mut(&|children| {
            children.reinstate_all();
        });
        self.siblings_mut(&|siblings| siblings.reinstate())
    }

    fn reinstate_child(
        &mut self,
        child_id: TokenId,
        previous_id: Option<TokenId>,
        next_id: Option<TokenId>,
    ) {
        self.children_mut(&|children| children.reinstate(child_id, previous_id, next_id));
    }

    pub fn replace(&mut self, replacement_id: TokenId) {
        let replaced_id = self.replace_self_and_descendants(replacement_id);
        self.parent_mut(&|parent| parent.replace(replaced_id, replacement_id));
    }

    fn replace_self_and_descendants(&mut self, replacement_id: TokenId) -> TokenId {
        self.children_mut(&|children| {
            children.replace_all(replacement_id);
        });
        self.siblings_mut(&|siblings| siblings.replace(replacement_id))
    }

    fn replace_child(&mut self, child_id: TokenId, replacement_id: TokenId) {
        self.children_mut(&|children| children.replace(child_id, replacement_id));
    }

    fn siblings<T: Default>(&self, action: &Fn(&Siblings) -> T) -> T {
        match *self {
            Token::Grapheme { ref siblings, .. } => action(siblings),
            Token::Word { ref siblings, .. } => action(siblings),
            Token::Line { ref siblings, .. } => action(siblings),
            Token::Paragraph { ref siblings, .. } => action(siblings),
            _ => T::default(),
        }
    }

    fn siblings_mut<T: Default>(&mut self, action: &Fn(&mut Siblings) -> T) -> T {
        match *self {
            Token::Grapheme {
                ref mut siblings, ..
            } => action(siblings),
            Token::Word {
                ref mut siblings, ..
            } => action(siblings),
            Token::Line {
                ref mut siblings, ..
            } => action(siblings),
            Token::Paragraph {
                ref mut siblings, ..
            } => action(siblings),
            _ => T::default(),
        }
    }

    fn siblings_or<T>(&self, default: T, action: &Fn(&Siblings) -> T) -> T {
        match *self {
            Token::Grapheme { ref siblings, .. } => action(siblings),
            Token::Word { ref siblings, .. } => action(siblings),
            Token::Line { ref siblings, .. } => action(siblings),
            Token::Paragraph { ref siblings, .. } => action(siblings),
            _ => default,
        }
    }

    fn parent_mut(&mut self, action: &Fn(&mut Parent)) {
        match *self {
            Token::Grapheme { ref mut parent, .. } => action(parent),
            Token::Word { ref mut parent, .. } => action(parent),
            Token::Line { ref mut parent, .. } => action(parent),
            _ => return,
        }
    }

    fn parent_or<T>(&self, default: T, action: &Fn(&Parent) -> T) -> T {
        match *self {
            Token::Grapheme { ref parent, .. } => action(parent),
            Token::Word { ref parent, .. } => action(parent),
            Token::Line { ref parent, .. } => action(parent),
            _ => default,
        }
    }

    fn children_mut(&mut self, action: &Fn(&mut Children)) {
        match *self {
            Token::Word {
                ref mut children, ..
            } => action(children),
            Token::Line {
                ref mut children, ..
            } => action(children),
            Token::Paragraph {
                ref mut children, ..
            } => action(children),
            _ => return,
        }
    }

    fn children_or<T>(&self, default: T, action: &Fn(&Children) -> T) -> T {
        match *self {
            Token::Word { ref children, .. } => action(children),
            Token::Line { ref children, .. } => action(children),
            Token::Paragraph { ref children, .. } => action(children),
            _ => default,
        }
    }

    fn value_or<'v, T>(&'v self, default: T, action: &Fn(&'v Value) -> T) -> T {
        match *self {
            Token::Grapheme { ref value, .. } => action(value),
            _ => default,
        }
    }
}

fn link_children(arena: Arc<RwLock<TokenArena>>, parent_id: TokenId, children: &[TokenId]) {
    children.iter().for_each(|child_id| {
        arena
            .read()
            .mutate(*child_id, &|mut child| child.set_parent(parent_id))
    });
}

impl Display for Token {
    fn fmt(&self, formatter: &mut Formatter) -> fmt::Result {
        match *self {
            Token::Grapheme { ref value, .. } => value.fmt(formatter),
            Token::Word { ref children, .. } => children.fmt(formatter),
            Token::Line { ref children, .. } => children.fmt(formatter),
            Token::Paragraph { ref children, .. } => children.fmt(formatter),
            Token::Null { .. } => write!(formatter, ""),
        }
    }
}

impl Eq for Token {}

impl PartialEq for Token {
    fn eq(&self, rhs: &Token) -> bool {
        self.siblings_or(false, &|s| rhs.siblings_or(false, &|rs| s == rs))
    }
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum TokenType {
    Null,
    Grapheme,
    Word,
    Line,
    Paragraph,
}

impl Default for TokenType {
    fn default() -> TokenType {
        TokenType::Null
    }
}

impl Display for TokenType {
    fn fmt(&self, formatter: &mut Formatter) -> fmt::Result {
        write!(
            formatter,
            "{}",
            match *self {
                TokenType::Null => "Null",
                TokenType::Grapheme => "Grapheme",
                TokenType::Word => "Word",
                TokenType::Line => "Line",
                TokenType::Paragraph => "Paragraph",
            }
        )
    }
}
