// Copyright © 2018, 2019 Phil Booth
//
// This file is part of pbvi.
//
// pbvi is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// pbvi is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// pbvi. If not, see <https://www.gnu.org/licenses/>.

#[cfg(test)]
mod test;

use std::{
    error::Error,
    fs, io,
    path::{Path, PathBuf},
    str,
    time::SystemTime,
};

use unicode_bom::Bom;

macro_rules! metadata {
    ($self:ident, $metadata:ident, $result:expr) => {{
        let $metadata = fs::metadata(&$self.path)?;
        $result
    }};
}

pub struct File {
    path: PathBuf,
}

impl File {
    pub fn new(path: &str) -> File {
        File {
            path: PathBuf::from(path),
        }
    }

    pub fn path(&self) -> &Path {
        &self.path
    }

    pub fn exists(&self) -> io::Result<bool> {
        match fs::metadata(&self.path) {
            Ok(_) => Ok(true),
            Err(error) => {
                if error.kind() == io::ErrorKind::NotFound {
                    Ok(false)
                } else {
                    Err(error)
                }
            }
        }
    }

    pub fn is_directory(&self) -> io::Result<bool> {
        metadata!(self, metadata, Ok(metadata.is_dir()))
    }

    pub fn last_modified_at(&self) -> io::Result<SystemTime> {
        metadata!(self, metadata, metadata.modified())
    }

    pub fn read(&self) -> io::Result<String> {
        let data = fs::read(&self.path)?;
        let bom = Bom::from(data.as_slice());
        match bom {
            // TODO: fall back to str::from_utf8_lossy in the Bom::Null case?
            Bom::Null => str::from_utf8(&data)
                .map_err(|e| decoding_error(e))
                .map(|s| s.to_string()),
            Bom::Utf8 => str::from_utf8(&data[bom.len()..])
                .map_err(|e| decoding_error(e))
                .map(|s| s.to_string()),
            // TODO: implement support for other encodings
            _ => Err(decoding_error(format!("{} BOM detected", bom))),
        }
    }
}

fn decoding_error<E>(reason: E) -> io::Error
where
    E: Into<Box<Error + Send + Sync>>,
{
    io::Error::new(io::ErrorKind::InvalidData, reason)
}
