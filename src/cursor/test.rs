// Copyright © 2018, 2019 Phil Booth
//
// This file is part of pbvi.
//
// pbvi is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// pbvi is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// pbvi. If not, see <https://www.gnu.org/licenses/>.

use super::*;
use crate::tokens::parse;

#[test]
fn grapheme_index() {
    let arena = Arc::new(RwLock::new(TokenArena::new()));

    let cursor = Cursor::new(arena.clone(), None, 0, 0);
    assert_eq!(cursor.grapheme_index(), 0);

    let cursor = Cursor::new(arena.clone(), None, 1, 0);
    assert_eq!(cursor.grapheme_index(), 1);

    let cursor = Cursor::new(arena.clone(), None, 2, 0);
    assert_eq!(cursor.grapheme_index(), 2);

    let cursor = Cursor::new(arena.clone(), None, 0, 1);
    assert_eq!(cursor.grapheme_index(), 0);
}

#[test]
fn line_index() {
    let arena = Arc::new(RwLock::new(TokenArena::new()));

    let cursor = Cursor::new(arena.clone(), None, 0, 0);
    assert_eq!(cursor.line_index(), 0);

    let cursor = Cursor::new(arena.clone(), None, 0, 1);
    assert_eq!(cursor.line_index(), 1);

    let cursor = Cursor::new(arena.clone(), None, 0, 2);
    assert_eq!(cursor.line_index(), 2);

    let cursor = Cursor::new(arena.clone(), None, 1, 0);
    assert_eq!(cursor.line_index(), 0);
}

#[test]
fn move_grapheme() {
    let arena = Arc::new(RwLock::new(TokenArena::new()));

    let grapheme_ids = parse::graphemes(arena.clone(), "012").unwrap();
    let mut cursor = Cursor::new(arena.clone(), Some(grapheme_ids[0]), 0, 0);

    cursor.move_grapheme(0);
    assert_eq!(cursor.grapheme_index(), 0);
    assert_eq!(cursor.line_index(), 0);

    cursor.move_grapheme(1);
    assert_eq!(cursor.grapheme_index(), 1);
    assert_eq!(cursor.line_index(), 0);

    cursor.move_grapheme(1);
    assert_eq!(cursor.grapheme_index(), 2);
    assert_eq!(cursor.line_index(), 0);

    cursor.move_grapheme(1);
    assert_eq!(cursor.grapheme_index(), 2);
    assert_eq!(cursor.line_index(), 0);

    cursor.move_grapheme(-1);
    assert_eq!(cursor.grapheme_index(), 1);
    assert_eq!(cursor.line_index(), 0);

    cursor.move_grapheme(-1);
    assert_eq!(cursor.grapheme_index(), 0);
    assert_eq!(cursor.line_index(), 0);

    cursor.move_grapheme(-1);
    assert_eq!(cursor.grapheme_index(), 0);
    assert_eq!(cursor.line_index(), 0);

    cursor.move_grapheme(2);
    assert_eq!(cursor.grapheme_index(), 2);
    assert_eq!(cursor.line_index(), 0);

    cursor.move_grapheme(2);
    assert_eq!(cursor.grapheme_index(), 2);
    assert_eq!(cursor.line_index(), 0);

    cursor.move_grapheme(-2);
    assert_eq!(cursor.grapheme_index(), 0);
    assert_eq!(cursor.line_index(), 0);

    cursor.move_grapheme(3);
    assert_eq!(cursor.grapheme_index(), 2);
    assert_eq!(cursor.line_index(), 0);
}

#[test]
fn move_grapheme_with_line_ending() {
    let arena = Arc::new(RwLock::new(TokenArena::new()));

    let line_ids = parse::lines(arena.clone(), "01\n01").unwrap();
    let word_id = arena
        .read()
        .read(line_ids[0], &|line| line.first_child().unwrap());
    let grapheme_id = arena.read().read(word_id, &|word| word.first_child());
    let mut cursor = Cursor::new(arena.clone(), grapheme_id, 0, 0);

    cursor.move_grapheme(2);
    assert_eq!(cursor.grapheme_index(), 1);
    assert_eq!(cursor.line_index(), 0);

    cursor.move_grapheme(1);
    assert_eq!(cursor.grapheme_index(), 1);
    assert_eq!(cursor.line_index(), 0);

    cursor.move_grapheme(-1);
    assert_eq!(cursor.grapheme_index(), 0);
    assert_eq!(cursor.line_index(), 0);

    cursor.move_line(1);
    assert_eq!(cursor.grapheme_index(), 0);
    assert_eq!(cursor.line_index(), 1);

    cursor.move_grapheme(1);
    assert_eq!(cursor.grapheme_index(), 1);
    assert_eq!(cursor.line_index(), 1);

    cursor.move_grapheme(1);
    assert_eq!(cursor.grapheme_index(), 1);
    assert_eq!(cursor.line_index(), 1);
}

#[test]
fn move_grapheme_with_paragraph_ending() {
    let arena = Arc::new(RwLock::new(TokenArena::new()));

    let paragraph_ids = parse::paragraphs(arena.clone(), "01\r\n\r\n01").unwrap();
    let line_id = arena.read().read(paragraph_ids[0], &|paragraph| {
        paragraph.first_child().unwrap()
    });
    let word_id = arena
        .read()
        .read(line_id, &|line| line.first_child().unwrap());
    let grapheme_id = arena.read().read(word_id, &|word| word.first_child());
    let mut cursor = Cursor::new(arena.clone(), grapheme_id, 0, 0);

    cursor.move_grapheme(1);
    assert_eq!(cursor.grapheme_index(), 1);
    assert_eq!(cursor.line_index(), 0);

    cursor.move_grapheme(1);
    assert_eq!(cursor.grapheme_index(), 1);
    assert_eq!(cursor.line_index(), 0);

    cursor.move_grapheme(-1);
    assert_eq!(cursor.grapheme_index(), 0);
    assert_eq!(cursor.line_index(), 0);

    cursor.move_line(1);
    assert_eq!(cursor.grapheme_index(), 0);
    assert_eq!(cursor.line_index(), 1);

    cursor.move_line(1);
    assert_eq!(cursor.grapheme_index(), 0);
    assert_eq!(cursor.line_index(), 2);

    cursor.move_grapheme(1);
    assert_eq!(cursor.grapheme_index(), 1);
    assert_eq!(cursor.line_index(), 2);

    cursor.move_grapheme(1);
    assert_eq!(cursor.grapheme_index(), 1);
    assert_eq!(cursor.line_index(), 2);
}

#[test]
fn move_word() {
    let arena = Arc::new(RwLock::new(TokenArena::new()));

    let line_ids = parse::lines(arena.clone(), "0 11, 222\n3333").unwrap();
    let word_id = arena
        .read()
        .read(line_ids[0], &|line| line.first_child().unwrap());
    let grapheme_id = arena.read().read(word_id, &|word| word.first_child());
    let mut cursor = Cursor::new(arena.clone(), grapheme_id, 0, 0);

    cursor.move_word(0);
    assert_eq!(cursor.grapheme_index(), 0);
    assert_eq!(cursor.line_index(), 0);

    cursor.move_word(1);
    assert_eq!(cursor.grapheme_index(), 2);
    assert_eq!(cursor.line_index(), 0);

    cursor.move_word(1);
    assert_eq!(cursor.grapheme_index(), 4);
    assert_eq!(cursor.line_index(), 0);

    cursor.move_word(1);
    assert_eq!(cursor.grapheme_index(), 6);
    assert_eq!(cursor.line_index(), 0);

    cursor.move_word(1);
    assert_eq!(cursor.grapheme_index(), 0);
    assert_eq!(cursor.line_index(), 1);

    cursor.move_word(1);
    assert_eq!(cursor.grapheme_index(), 0);
    assert_eq!(cursor.line_index(), 1);

    cursor.move_word(-1);
    assert_eq!(cursor.grapheme_index(), 6);
    assert_eq!(cursor.line_index(), 0);

    cursor.move_word(-1);
    assert_eq!(cursor.grapheme_index(), 4);
    assert_eq!(cursor.line_index(), 0);

    cursor.move_word(-1);
    assert_eq!(cursor.grapheme_index(), 2);
    assert_eq!(cursor.line_index(), 0);

    cursor.move_word(-1);
    assert_eq!(cursor.grapheme_index(), 0);
    assert_eq!(cursor.line_index(), 0);

    cursor.move_word(-1);
    assert_eq!(cursor.grapheme_index(), 0);
    assert_eq!(cursor.line_index(), 0);

    cursor.move_word(3);
    assert_eq!(cursor.grapheme_index(), 6);
    assert_eq!(cursor.line_index(), 0);

    cursor.move_word(3);
    assert_eq!(cursor.grapheme_index(), 0);
    assert_eq!(cursor.line_index(), 1);

    cursor.move_word(3);
    assert_eq!(cursor.grapheme_index(), 0);
    assert_eq!(cursor.line_index(), 1);

    cursor.move_word(-1);
    assert_eq!(cursor.grapheme_index(), 6);
    assert_eq!(cursor.line_index(), 0);

    cursor.move_grapheme(1);
    assert_eq!(cursor.grapheme_index(), 7);
    assert_eq!(cursor.line_index(), 0);

    cursor.move_word(-1);
    assert_eq!(cursor.grapheme_index(), 6);
    assert_eq!(cursor.line_index(), 0);

    cursor.move_word(-1);
    assert_eq!(cursor.grapheme_index(), 4);
    assert_eq!(cursor.line_index(), 0);
}

#[test]
fn move_to_word_end() {
    let arena = Arc::new(RwLock::new(TokenArena::new()));

    let line_ids = parse::lines(arena.clone(), "00 111, 2222\n33333").unwrap();
    let word_id = arena
        .read()
        .read(line_ids[0], &|line| line.first_child().unwrap());
    let grapheme_id = arena.read().read(word_id, &|word| word.first_child());
    let mut cursor = Cursor::new(arena.clone(), grapheme_id, 0, 0);

    cursor.move_to_word_end(1);
    assert_eq!(cursor.grapheme_index(), 1);
    assert_eq!(cursor.line_index(), 0);

    cursor.move_line(1);
    assert_eq!(cursor.grapheme_index(), 1);
    assert_eq!(cursor.line_index(), 1);

    cursor.move_line(-1);
    assert_eq!(cursor.grapheme_index(), 1);
    assert_eq!(cursor.line_index(), 0);

    cursor.move_to_word_end(1);
    assert_eq!(cursor.grapheme_index(), 5);
    assert_eq!(cursor.line_index(), 0);

    cursor.move_to_word_end(1);
    assert_eq!(cursor.grapheme_index(), 6);
    assert_eq!(cursor.line_index(), 0);

    cursor.move_to_word_end(1);
    assert_eq!(cursor.grapheme_index(), 11);
    assert_eq!(cursor.line_index(), 0);

    cursor.move_to_word_end(1);
    assert_eq!(cursor.grapheme_index(), 4);
    assert_eq!(cursor.line_index(), 1);

    cursor.move_to_word_end(1);
    assert_eq!(cursor.grapheme_index(), 4);
    assert_eq!(cursor.line_index(), 1);

    cursor.move_to_word_end(-1);
    assert_eq!(cursor.grapheme_index(), 11);
    assert_eq!(cursor.line_index(), 0);

    cursor.move_to_word_end(-1);
    assert_eq!(cursor.grapheme_index(), 6);
    assert_eq!(cursor.line_index(), 0);

    cursor.move_to_word_end(-1);
    assert_eq!(cursor.grapheme_index(), 5);
    assert_eq!(cursor.line_index(), 0);

    cursor.move_to_word_end(-1);
    assert_eq!(cursor.grapheme_index(), 1);
    assert_eq!(cursor.line_index(), 0);

    cursor.move_to_word_end(-1);
    assert_eq!(cursor.grapheme_index(), 1);
    assert_eq!(cursor.line_index(), 0);

    cursor.move_to_word_end(1);
    assert_eq!(cursor.grapheme_index(), 5);
    assert_eq!(cursor.line_index(), 0);

    cursor.move_grapheme(-1);
    assert_eq!(cursor.grapheme_index(), 4);
    assert_eq!(cursor.line_index(), 0);

    cursor.move_to_word_end(1);
    assert_eq!(cursor.grapheme_index(), 5);
    assert_eq!(cursor.line_index(), 0);
}

#[test]
fn move_line() {
    let arena = Arc::new(RwLock::new(TokenArena::new()));

    let line_ids = parse::lines(arena.clone(), "000\n11\n2\n33333").unwrap();
    let word_id = arena
        .read()
        .read(line_ids[0], &|line| line.first_child().unwrap());
    let grapheme_id = arena.read().read(word_id, &|word| word.first_child());
    let mut cursor = Cursor::new(arena.clone(), grapheme_id, 2, 0);

    cursor.move_line(0);
    assert_eq!(cursor.grapheme_index(), 2);
    assert_eq!(cursor.line_index(), 0);

    cursor.move_line(1);
    assert_eq!(cursor.grapheme_index(), 1);
    assert_eq!(cursor.line_index(), 1);

    cursor.move_line(1);
    assert_eq!(cursor.grapheme_index(), 0);
    assert_eq!(cursor.line_index(), 2);

    cursor.move_line(1);
    assert_eq!(cursor.grapheme_index(), 2);
    assert_eq!(cursor.line_index(), 3);

    cursor.move_line(1);
    assert_eq!(cursor.grapheme_index(), 2);
    assert_eq!(cursor.line_index(), 3);

    cursor.move_line(-1);
    assert_eq!(cursor.grapheme_index(), 0);
    assert_eq!(cursor.line_index(), 2);

    cursor.move_line(-1);
    assert_eq!(cursor.grapheme_index(), 1);
    assert_eq!(cursor.line_index(), 1);

    cursor.move_line(-1);
    assert_eq!(cursor.grapheme_index(), 2);
    assert_eq!(cursor.line_index(), 0);

    cursor.move_line(-1);
    assert_eq!(cursor.grapheme_index(), 2);
    assert_eq!(cursor.line_index(), 0);

    cursor.move_line(2);
    assert_eq!(cursor.grapheme_index(), 0);
    assert_eq!(cursor.line_index(), 2);

    cursor.move_line(2);
    assert_eq!(cursor.grapheme_index(), 2);
    assert_eq!(cursor.line_index(), 3);
}

#[test]
fn move_to_line_end() {
    let arena = Arc::new(RwLock::new(TokenArena::new()));

    let line_ids = parse::lines(arena.clone(), "00\n111\n2222").unwrap();
    let word_id = arena
        .read()
        .read(line_ids[0], &|line| line.first_child().unwrap());
    let grapheme_id = arena.read().read(word_id, &|word| word.first_child());
    let mut cursor = Cursor::new(arena.clone(), grapheme_id, 0, 0);

    cursor.move_to_line_end();
    assert_eq!(cursor.grapheme_index(), 1);
    assert_eq!(cursor.line_index(), 0);

    cursor.move_line(1);
    assert_eq!(cursor.grapheme_index(), 2);
    assert_eq!(cursor.line_index(), 1);

    cursor.move_line(1);
    assert_eq!(cursor.grapheme_index(), 3);
    assert_eq!(cursor.line_index(), 2);

    cursor.move_line(-1);
    assert_eq!(cursor.grapheme_index(), 2);
    assert_eq!(cursor.line_index(), 1);

    cursor.move_line(1);
    assert_eq!(cursor.grapheme_index(), 3);
    assert_eq!(cursor.line_index(), 2);
}

#[test]
fn move_paragraph() {
    let arena = Arc::new(RwLock::new(TokenArena::new()));

    let paragraph_ids =
        parse::paragraphs(arena.clone(), "000\n111\n2222\n\n333\n444\n\n\n555").unwrap();
    let line_id = arena.read().read(paragraph_ids[0], &|paragraph| {
        paragraph.first_child().unwrap()
    });
    let word_id = arena
        .read()
        .read(line_id, &|line| line.first_child().unwrap());
    let grapheme_id = arena.read().read(word_id, &|word| word.first_child());
    let mut cursor = Cursor::new(arena.clone(), grapheme_id, 0, 0);

    cursor.move_paragraph(0);
    assert_eq!(cursor.grapheme_index(), 0);
    assert_eq!(cursor.line_index(), 0);

    cursor.move_paragraph(1);
    assert_eq!(cursor.grapheme_index(), 0);
    assert_eq!(cursor.line_index(), 3);

    cursor.move_paragraph(1);
    assert_eq!(cursor.grapheme_index(), 0);
    assert_eq!(cursor.line_index(), 6);

    cursor.move_paragraph(1);
    assert_eq!(cursor.grapheme_index(), 2);
    assert_eq!(cursor.line_index(), 8);

    cursor.move_paragraph(1);
    assert_eq!(cursor.grapheme_index(), 2);
    assert_eq!(cursor.line_index(), 8);

    cursor.move_paragraph(-1);
    assert_eq!(cursor.grapheme_index(), 0);
    assert_eq!(cursor.line_index(), 7);

    cursor.move_paragraph(-1);
    assert_eq!(cursor.grapheme_index(), 0);
    assert_eq!(cursor.line_index(), 3);

    cursor.move_paragraph(-1);
    assert_eq!(cursor.grapheme_index(), 0);
    assert_eq!(cursor.line_index(), 0);

    cursor.move_paragraph(-1);
    assert_eq!(cursor.grapheme_index(), 0);
    assert_eq!(cursor.line_index(), 0);

    cursor.move_paragraph(2);
    assert_eq!(cursor.grapheme_index(), 0);
    assert_eq!(cursor.line_index(), 6);

    cursor.move_paragraph(2);
    assert_eq!(cursor.grapheme_index(), 2);
    assert_eq!(cursor.line_index(), 8);

    cursor.move_paragraph(2);
    assert_eq!(cursor.grapheme_index(), 2);
    assert_eq!(cursor.line_index(), 8);
}
