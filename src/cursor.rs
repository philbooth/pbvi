// Copyright © 2018, 2019 Phil Booth
//
// This file is part of pbvi.
//
// pbvi is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// pbvi is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// pbvi. If not, see <https://www.gnu.org/licenses/>.

#[cfg(test)]
mod test;

use std::sync::Arc;

use parking_lot::{RwLock, RwLockReadGuard};

use crate::tokens::{Token, TokenArena, TokenId};

macro_rules! simple_move {
    ($self:ident, $delta:ident, $id:expr, $index:ident, $move_absolute:ident) => {
        if let Some(id) = $id {
            let ids = $self.arena().read(id, &|token| {
                if $delta >= 0 {
                    token.following_siblings()
                } else {
                    token.preceding_siblings()
                }
            });

            if $delta >= 0 {
                $self.$index += $self.$move_absolute($delta as usize, ids);
            } else {
                $self.$index -= $self.$move_absolute((0 - $delta) as usize, ids.rev());
            }
        }
    };
}

#[derive(Debug)]
pub struct Cursor {
    arena: Arc<RwLock<TokenArena>>,
    grapheme_id: Option<TokenId>,
    grapheme_index: usize,
    ideal_grapheme_index: usize,
    line_index: usize,
    stick_to_line_end: bool,
}

impl Cursor {
    pub fn new(
        arena: Arc<RwLock<TokenArena>>,
        grapheme_id: Option<TokenId>,
        grapheme_index: usize,
        line_index: usize,
    ) -> Self {
        Self {
            arena,
            grapheme_id,
            grapheme_index,
            ideal_grapheme_index: grapheme_index,
            line_index,
            stick_to_line_end: false,
        }
    }

    pub fn grapheme_index(&self) -> usize {
        self.grapheme_index
    }

    pub fn line_index(&self) -> usize {
        self.line_index
    }

    pub fn move_grapheme(&mut self, delta: isize) {
        self.stick_to_line_end = false;

        simple_move!(
            self,
            delta,
            self.grapheme_id,
            grapheme_index,
            move_grapheme_absolute
        );

        self.ideal_grapheme_index = self.grapheme_index;
    }

    fn move_grapheme_absolute<I>(&mut self, absolute_delta: usize, grapheme_ids: I) -> usize
    where
        I: Iterator<Item = TokenId>,
    {
        let mut count = 0;

        for grapheme_id in grapheme_ids {
            if count == absolute_delta {
                break;
            }

            if self
                .arena()
                .read(grapheme_id, &|grapheme| grapheme.is_newline_grapheme())
            {
                break;
            }

            self.grapheme_id = Some(grapheme_id);
            count += 1;
        }

        count
    }

    fn arena(&self) -> RwLockReadGuard<TokenArena> {
        self.arena.read()
    }

    pub fn move_word(&mut self, delta: isize) {
        self.stick_to_line_end = false;

        if let Some(word_id) = self.word_id() {
            let word_ids = self.arena().read(word_id, &|word| {
                if delta >= 0 {
                    word.following_siblings()
                } else {
                    if self.grapheme_id.unwrap() == word.first_child().unwrap() {
                        word.preceding_siblings()
                    } else {
                        word.iter(None, None)
                    }
                }
            });

            if delta >= 0 {
                self.line_index +=
                    self.move_word_absolute(delta as usize, word_ids, &|word| word.first_child());
            } else {
                self.line_index -=
                    self.move_word_absolute((0 - delta) as usize, word_ids.rev(), &|word| {
                        word.first_child()
                    });
            }

            self.ideal_grapheme_index = self.grapheme_index;
        }
    }

    fn word_id(&self) -> Option<TokenId> {
        if let Some(grapheme_id) = self.grapheme_id {
            self.arena()
                .read(grapheme_id, &|grapheme| grapheme.parent())
        } else {
            None
        }
    }

    fn move_word_absolute<I>(
        &mut self,
        absolute_delta: usize,
        word_ids: I,
        position: &Fn(&Token) -> Option<TokenId>,
    ) -> usize
    where
        I: Iterator<Item = TokenId>,
    {
        let mut new_word_id = None;
        let mut count = 0;
        let mut line_count = 0;

        for word_id in word_ids {
            if count == absolute_delta {
                break;
            }

            let (is_newline, is_space) = self
                .arena()
                .read(word_id, &|word| (word.is_newline_word(), word.is_space()));

            if is_newline {
                line_count += 1;
            } else if !is_space {
                count += 1;
            }

            new_word_id = Some(word_id);
        }

        if let Some(new_word_id) = new_word_id {
            let (grapheme_id, line_id) = self
                .arena()
                .read(new_word_id, &|word| (position(&word), word.parent()));
            self.move_to_grapheme_on_line(grapheme_id, line_id);
        }

        line_count
    }

    fn move_to_grapheme_on_line(&mut self, grapheme_id: Option<TokenId>, line_id: Option<TokenId>) {
        self.grapheme_id = grapheme_id;
        self.grapheme_index = 0;

        if let Some(grapheme_id) = grapheme_id {
            if let Some(line_id) = line_id {
                let first_word_id = self
                    .arena()
                    .read(line_id, &|line| line.first_child().unwrap());
                let first_grapheme_id = self
                    .arena()
                    .read(first_word_id, &|word| word.first_child().unwrap());

                if grapheme_id != first_grapheme_id {
                    let second_grapheme_id = self
                        .arena()
                        .read(first_grapheme_id, &|grapheme| grapheme.next().unwrap());

                    if grapheme_id == second_grapheme_id {
                        self.grapheme_index = 1;
                    } else {
                        let grapheme_ids = self.arena().read(second_grapheme_id, &|grapheme| {
                            grapheme.iter(None, Some(grapheme_id))
                        });
                        let line_length = self.arena().read(line_id, &|line| line.len());
                        self.grapheme_index =
                            self.move_grapheme_absolute(line_length, grapheme_ids);
                    }
                }
            }
        }
    }

    pub fn move_to_word_end(&mut self, delta: isize) {
        self.stick_to_line_end = false;

        if let Some(word_id) = self.word_id() {
            let word_ids = self.arena().read(word_id, &|word| {
                if delta >= 0 {
                    if self.grapheme_id.unwrap() == word.last_child().unwrap() {
                        word.following_siblings()
                    } else {
                        word.iter(None, None)
                    }
                } else {
                    word.preceding_siblings()
                }
            });

            if delta >= 0 {
                self.line_index +=
                    self.move_word_absolute(delta as usize, word_ids, &|word| word.last_child());
            } else {
                self.line_index -=
                    self.move_word_absolute((0 - delta) as usize, word_ids.rev(), &|word| {
                        word.last_child()
                    });
            }

            self.ideal_grapheme_index = self.grapheme_index;
        }
    }

    pub fn move_line(&mut self, delta: isize) {
        simple_move!(self, delta, self.line_id(), line_index, move_line_absolute);

        if self.stick_to_line_end {
            self.move_to_line_end();
        }
    }

    fn line_id(&self) -> Option<TokenId> {
        if let Some(word_id) = self.word_id() {
            self.arena().read(word_id, &|word| word.parent())
        } else {
            None
        }
    }

    fn move_line_absolute<I>(&mut self, absolute_delta: usize, line_ids: I) -> usize
    where
        I: Iterator<Item = TokenId>,
    {
        let mut new_line_id = None;
        let mut count = 0;

        for line_id in line_ids {
            if count == absolute_delta {
                break;
            }

            new_line_id = Some(line_id);
            count += 1;
        }

        if let Some(new_line_id) = new_line_id {
            let word_id = self.arena().read(new_line_id, &|line| line.first_child());

            if let Some(word_id) = word_id {
                let arena = self.arena.clone();
                self.grapheme_id = arena.read().read(word_id, &|word| word.first_child());

                if let Some(grapheme_id) = self.grapheme_id {
                    let grapheme_index = self.ideal_grapheme_index;
                    let grapheme_ids = arena
                        .read()
                        .read(grapheme_id, &|grapheme| grapheme.following_siblings());
                    self.grapheme_index = self.move_grapheme_absolute(grapheme_index, grapheme_ids);
                }
            }
        }

        count
    }

    pub fn move_to_line_end(&mut self) {
        if let Some(line_id) = self.line_id() {
            let line_length = self.arena().read(line_id, &|line| line.len());
            if line_length > self.grapheme_index + 1 {
                let delta = line_length - self.grapheme_index - 1;
                self.move_grapheme(delta as isize);
            }
        }

        self.stick_to_line_end = true;
    }

    pub fn move_paragraph(&mut self, delta: isize) {
        if delta == 0 {
            return;
        }

        if let Some(line_id) = self.line_id() {
            let line_ids = self.arena().read(line_id, &|line| {
                if delta > 0 {
                    line.following_siblings()
                } else {
                    line.preceding_siblings()
                }
            });

            let next_delta = if delta > 0 {
                self.line_index += self.move_paragraph_absolute(line_ids);
                self.move_to_line_end();
                delta - 1
            } else {
                self.line_index -= self.move_paragraph_absolute(line_ids.rev());
                delta + 1
            };

            self.move_paragraph(next_delta);
        }
    }

    fn move_paragraph_absolute<I>(&mut self, line_ids: I) -> usize
    where
        I: Iterator<Item = TokenId>,
    {
        let mut count = 0;
        let mut new_line_id = None;
        let mut started = true;

        if let Some(line_id) = self.line_id() {
            if self.arena().read(line_id, &|line| line.is_newline_line()) {
                // Seek past current blank lines before looking for the next one
                started = false;
            }
        }

        for line_id in line_ids {
            count += 1;
            new_line_id = Some(line_id);

            if self.arena().read(line_id, &|line| line.is_newline_line()) {
                if started {
                    break;
                }
            } else if !started {
                started = true;
            }
        }

        if let Some(new_line_id) = new_line_id {
            let word_id = self
                .arena()
                .read(new_line_id, &|line| line.first_child().unwrap());
            let arena = self.arena.clone();
            self.grapheme_id = arena.read().read(word_id, &|word| word.first_child());
            self.grapheme_index = 0;
        }

        count
    }
}
