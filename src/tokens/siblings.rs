// Copyright © 2018, 2019 Phil Booth
//
// This file is part of pbvi.
//
// pbvi is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// pbvi is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// pbvi. If not, see <https://www.gnu.org/licenses/>.

use std::sync::Arc;

use parking_lot::{RwLock, RwLockReadGuard};

use super::{TokenArena, TokenId, TokenIterator, TokenType};

#[derive(Debug)]
pub struct Siblings {
    arena: Arc<RwLock<TokenArena>>,
    this: TokenId,
    previous: Option<TokenId>,
    next: Option<TokenId>,
    terminates: TokenType,
    default_terminates: TokenType,
}

impl Siblings {
    pub fn new(arena: Arc<RwLock<TokenArena>>, id: TokenId, terminates: TokenType) -> Self {
        Self {
            arena,
            this: id,
            previous: None,
            next: None,
            terminates,
            default_terminates: terminates,
        }
    }

    pub fn iter(&self, first: Option<TokenId>, last: Option<TokenId>) -> TokenIterator {
        TokenIterator::new(self.arena.clone(), Some(self.this), first, last)
    }

    pub fn terminates(&self) -> TokenType {
        self.terminates
    }

    pub fn set_terminates(&mut self, terminates: TokenType) {
        self.terminates = terminates;
    }

    pub fn clear_terminates(&mut self) {
        self.terminates = self.default_terminates;
    }

    pub fn this(&self) -> TokenId {
        self.this
    }

    pub fn previous(&self) -> Option<TokenId> {
        self.previous
    }

    pub fn set_previous(&mut self, previous_id: TokenId) {
        self.previous = Some(previous_id);
    }

    pub fn set_previous_and_cascade(&mut self, previous_id: TokenId) {
        self.set_previous(previous_id);

        self.arena().mutate(previous_id, &|mut previous| {
            previous.set_next(self.this);
        });
    }

    fn arena(&self) -> RwLockReadGuard<TokenArena> {
        self.arena.read()
    }

    pub fn clear_previous(&mut self) {
        self.previous = None;
    }

    pub fn next(&self) -> Option<TokenId> {
        self.next
    }

    pub fn set_next(&mut self, next_id: TokenId) {
        self.next = Some(next_id);
    }

    pub fn set_next_and_cascade(&mut self, next_id: TokenId) {
        self.set_next(next_id);

        self.arena().mutate(next_id, &|mut next| {
            next.set_previous(self.this);
        });
    }

    pub fn clear_next(&mut self) {
        self.next = None;
    }

    pub fn delete(&mut self) -> TokenId {
        if let Some(previous_id) = self.previous {
            if let Some(next_id) = self.next {
                self.arena().mutate(previous_id, &|mut previous| {
                    previous.set_next_and_cascade(next_id);
                });
            } else {
                self.arena().mutate(previous_id, &|mut previous| {
                    previous.clear_next();
                });
            }
        } else if let Some(next_id) = self.next {
            self.arena().mutate(next_id, &|mut next| {
                next.clear_previous();
            });
        }

        self.this
    }

    pub fn reinstate(&mut self) -> TokenId {
        if let Some(previous_id) = self.previous {
            self.arena().mutate(previous_id, &|mut previous| {
                previous.set_next(self.this);
            });
        }

        if let Some(next_id) = self.next {
            self.arena().mutate(next_id, &|mut next| {
                next.set_previous(self.this);
            });
        }

        self.this
    }

    pub fn replace(&mut self, replacement_id: TokenId) -> TokenId {
        if let Some(previous_id) = self.previous {
            self.arena().mutate(previous_id, &|mut previous| {
                previous.set_next_and_cascade(replacement_id);
            });
        }

        if let Some(next_id) = self.next {
            self.arena().mutate(next_id, &|mut next| {
                next.set_previous_and_cascade(replacement_id);
            });
        }

        self.this
    }
}

impl Eq for Siblings {}

impl PartialEq for Siblings {
    fn eq(&self, rhs: &Siblings) -> bool {
        self.this == rhs.this
    }
}
