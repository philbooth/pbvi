// Copyright © 2018, 2019 Phil Booth
//
// This file is part of pbvi.
//
// pbvi is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// pbvi is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// pbvi. If not, see <https://www.gnu.org/licenses/>.

use std::sync::Arc;

use parking_lot::{RwLock, RwLockReadGuard};

use super::{TokenArena, TokenId};

#[derive(Debug)]
pub struct Parent {
    arena: Arc<RwLock<TokenArena>>,
    parent: Option<TokenId>,
}

impl Parent {
    pub fn new(arena: Arc<RwLock<TokenArena>>) -> Self {
        Self {
            arena,
            parent: None,
        }
    }

    pub fn get(&self) -> Option<TokenId> {
        self.parent
    }

    pub fn set(&mut self, parent_id: TokenId) {
        self.parent = Some(parent_id);
    }

    pub fn delete(
        &mut self,
        child_id: TokenId,
        previous_id: Option<TokenId>,
        next_id: Option<TokenId>,
    ) {
        if let Some(parent_id) = self.parent {
            self.arena().mutate(parent_id, &|mut parent| {
                parent.delete_child(child_id, previous_id, next_id);
            });
        }
    }

    fn arena(&self) -> RwLockReadGuard<TokenArena> {
        self.arena.read()
    }

    pub fn reinstate(
        &mut self,
        child_id: TokenId,
        previous_id: Option<TokenId>,
        next_id: Option<TokenId>,
    ) {
        if let Some(parent_id) = self.parent {
            self.arena().mutate(parent_id, &|mut parent| {
                parent.reinstate_child(child_id, previous_id, next_id);
            });
        }
    }

    pub fn replace(&mut self, child_id: TokenId, replacement_id: TokenId) {
        if let Some(parent_id) = self.parent {
            self.arena().mutate(parent_id, &|mut parent| {
                parent.replace_child(child_id, replacement_id);
            });
        }
    }
}
