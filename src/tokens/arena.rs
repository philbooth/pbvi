// Copyright © 2018, 2019 Phil Booth
//
// This file is part of pbvi.
//
// pbvi is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// pbvi is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// pbvi. If not, see <https://www.gnu.org/licenses/>.

use std::{mem::replace, sync::Arc};

use parking_lot::{RwLock, RwLockReadGuard, RwLockWriteGuard};

use super::{Token, TokenError, TokenResult};

#[derive(Debug)]
pub struct TokenArena {
    tokens: Vec<RwLock<Token>>,
}

impl TokenArena {
    pub fn new() -> Self {
        Self { tokens: Vec::new() }
    }

    pub fn reserve_id(&mut self, arena: Arc<RwLock<Self>>) -> usize {
        self.tokens.push(RwLock::new(Token::Null { arena }));
        self.tokens.len() - 1
    }

    pub fn set(&mut self, id: usize, token: Token) -> TokenResult<()> {
        if id >= self.tokens.len() {
            Err(TokenError::invalid_id("set"))
        } else {
            replace(&mut self.tokens[id], RwLock::new(token));
            Ok(())
        }
    }

    pub fn read<T>(&self, id: usize, action: &Fn(RwLockReadGuard<Token>) -> T) -> T {
        self.get(id).map(action).unwrap()
    }

    pub fn get(&self, id: usize) -> TokenResult<RwLockReadGuard<Token>> {
        if id >= self.tokens.len() {
            Err(TokenError::invalid_id("get"))
        } else {
            Ok(self.tokens[id].read())
        }
    }

    pub fn mutate(&self, id: usize, action: &Fn(RwLockWriteGuard<Token>)) {
        self.get_mut(id).map(action).unwrap()
    }

    fn get_mut(&self, id: usize) -> TokenResult<RwLockWriteGuard<Token>> {
        if id >= self.tokens.len() {
            Err(TokenError::invalid_id("get_mut"))
        } else {
            Ok(self.tokens[id].write())
        }
    }
}
