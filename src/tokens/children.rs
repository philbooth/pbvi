// Copyright © 2018, 2019 Phil Booth
//
// This file is part of pbvi.
//
// pbvi is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// pbvi is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// pbvi. If not, see <https://www.gnu.org/licenses/>.

use std::{
    fmt::{self, Display, Formatter},
    sync::Arc,
};

use parking_lot::{RwLock, RwLockReadGuard};

use super::{TokenArena, TokenId, TokenIterator, TokenType};

#[derive(Debug)]
pub struct Children {
    arena: Arc<RwLock<TokenArena>>,
    first: Option<TokenId>,
    last: Option<TokenId>,
}

impl Children {
    pub fn new(
        arena: Arc<RwLock<TokenArena>>,
        children: &[TokenId],
        terminates: TokenType,
    ) -> Self {
        if let Some(last_id) = children.last() {
            arena.read().mutate(*last_id, &|mut last| {
                last.set_terminates(terminates);
            });
        }

        Self {
            arena,
            first: children.first().cloned(),
            last: children.last().cloned(),
        }
    }

    pub fn len(&self) -> usize {
        self.iter().fold(0, |sum, child_id| {
            sum + self.arena().read(child_id, &|child| child.len())
        })
    }

    fn arena(&self) -> RwLockReadGuard<TokenArena> {
        self.arena.read()
    }

    pub fn iter(&self) -> TokenIterator {
        if let Some(first_id) = self.first {
            self.arena()
                .read(first_id, &|first| first.iter(self.first, self.last))
        } else {
            TokenIterator::null(self.arena.clone())
        }
    }

    pub fn first(&self) -> Option<TokenId> {
        self.first
    }

    pub fn last(&self) -> Option<TokenId> {
        self.last
    }

    pub fn set_terminates(&mut self, terminates: TokenType) {
        if let Some(last_id) = self.last {
            self.arena().mutate(last_id, &|mut last| {
                last.set_terminates(terminates);
            });
        }
    }

    pub fn set_previous_and_cascade(&mut self, previous_id: TokenId) {
        if let Some(first_id) = self.first {
            self.arena().mutate(previous_id, &|mut previous| {
                previous.set_next_and_cascade(first_id);
            });
        }
    }

    pub fn set_child_previous_and_cascade(&mut self, previous_id: TokenId) {
        if let Some(first_id) = self.first {
            self.arena().mutate(previous_id, &|mut previous| {
                previous.set_child_next_and_cascade(first_id);
            });
        }
    }

    pub fn set_next_and_cascade(&mut self, next_id: TokenId) {
        if let Some(last_id) = self.last {
            self.arena().mutate(next_id, &|mut next| {
                next.set_previous_and_cascade(last_id);
            });
        }
    }

    pub fn set_child_next_and_cascade(&mut self, next_id: TokenId) {
        if let Some(last_id) = self.last {
            self.arena().mutate(next_id, &|mut next| {
                next.set_child_previous_and_cascade(last_id);
            });
        }
    }

    pub fn delete(
        &mut self,
        child_id: TokenId,
        previous_id: Option<TokenId>,
        next_id: Option<TokenId>,
    ) {
        let mut is_first_child = false;
        let mut is_last_child = false;

        if let Some(first_id) = self.first {
            if child_id == first_id {
                is_first_child = true;
            }
        }

        if let Some(last_id) = self.last {
            if child_id == last_id {
                is_last_child = true;
            }
        }

        if is_first_child {
            if is_last_child {
                self.first = None;
                self.last = None;
            } else {
                self.first = next_id;
            }
        } else if is_last_child {
            self.last = previous_id;
        }
    }

    pub fn delete_all(&mut self) {
        self.iter().for_each(|child_id| {
            self.arena().mutate(child_id, &|mut child| {
                child.delete_self_and_descendants();
            });
        });
    }

    pub fn reinstate(
        &mut self,
        child_id: TokenId,
        previous_id: Option<TokenId>,
        next_id: Option<TokenId>,
    ) {
        let mut is_first_child = false;
        let mut is_last_child = false;

        if let Some(first_id) = self.first {
            if let Some(next_id) = next_id {
                if first_id == next_id {
                    is_first_child = true;
                }
            } else {
                is_last_child = true;
            }
        }

        if let Some(last_id) = self.last {
            if let Some(previous_id) = previous_id {
                if last_id == previous_id {
                    is_last_child = true;
                }
            } else {
                is_first_child = true;
            }
        }

        if is_first_child {
            self.first = Some(child_id);
        }

        if is_last_child {
            self.last = Some(child_id);
        }
    }

    pub fn reinstate_all(&mut self) {
        self.iter().for_each(|child_id| {
            self.arena().mutate(child_id, &|mut child| {
                child.reinstate_self_and_descendants();
            });
        });
    }

    pub fn replace(&mut self, child_id: TokenId, replacement_id: TokenId) {
        let mut is_first_child = false;
        let mut is_last_child = false;

        if let Some(first_id) = self.first {
            if child_id == first_id {
                is_first_child = true;
            }
        }

        if let Some(last_id) = self.last {
            if child_id == last_id {
                is_last_child = true;
            }
        }

        if is_first_child {
            self.first = Some(replacement_id);

            if is_last_child {
                self.last = Some(replacement_id);
            }
        } else if is_last_child {
            self.last = Some(replacement_id);
        }
    }

    pub fn replace_all(&mut self, replacement_id: TokenId) {
        let (first, last) = {
            let lock = self.arena.clone();
            let arena = lock.read();
            let replacement = arena.get(replacement_id).unwrap();
            (replacement.first_child(), replacement.last_child())
        };
        self.first = first;
        self.last = last;
    }
}

impl Display for Children {
    fn fmt(&self, formatter: &mut Formatter) -> fmt::Result {
        write!(
            formatter,
            "{}",
            &self
                .iter()
                .map(|child_id| self.arena().read(child_id, &|child| format!("{}", *child)))
                .collect::<Vec<String>>()
                .concat()
        )
    }
}
