// Copyright © 2018, 2019 Phil Booth
//
// This file is part of pbvi.
//
// pbvi is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// pbvi is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// pbvi. If not, see <https://www.gnu.org/licenses/>.

mod lines_with_endings;

use std::sync::Arc;

use lazy_static::lazy_static;
use parking_lot::RwLock;
use regex::Regex;
use unicode_segmentation::UnicodeSegmentation;

use self::lines_with_endings::LinesWithEndings;
use super::{Token, TokenArena, TokenId, TokenResult};

lazy_static! {
    static ref PARAGRAPH_SEPARATOR: Regex =
        Regex::new(r"\r?\n\r?\n(?:\r?\n)*").expect("Regex error");
}

pub fn graphemes(arena: Arc<RwLock<TokenArena>>, values: &str) -> TokenResult<Vec<TokenId>> {
    create_tokens(arena, values.graphemes(true), &|arena, id, value| {
        Ok(Token::new_grapheme(arena, id, value))
    })
}

fn create_tokens<I, S>(
    arena: Arc<RwLock<TokenArena>>,
    values: I,
    factory: &Fn(Arc<RwLock<TokenArena>>, TokenId, &str) -> TokenResult<Token>,
) -> TokenResult<Vec<TokenId>>
where
    I: Iterator<Item = S>,
    S: AsRef<str>,
{
    let mut ids = Vec::new();

    for value in values {
        let id = arena.write().reserve_id(arena.clone());
        let token = factory(arena.clone(), id, value.as_ref())?;
        arena.write().set(id, token)?;
        link_token(arena.clone(), id, &ids);
        ids.push(id);
    }

    Ok(ids)
}

fn link_token(arena: Arc<RwLock<TokenArena>>, current_id: TokenId, ids: &[TokenId]) {
    let count = ids.len();

    if count > 0 {
        arena.read().mutate(current_id, &|mut current| {
            current.set_previous_and_cascade(ids[count - 1]);
        });
    }
}

pub fn words(arena: Arc<RwLock<TokenArena>>, values: &str) -> TokenResult<Vec<TokenId>> {
    create_tokens(arena, values.split_word_bounds(), &|arena, id, value| {
        Ok(Token::new_word(
            arena.clone(),
            id,
            &graphemes(arena.clone(), value)?,
        ))
    })
}

pub fn lines(arena: Arc<RwLock<TokenArena>>, values: &str) -> TokenResult<Vec<TokenId>> {
    create_tokens(
        arena,
        LinesWithEndings::from(values),
        &|arena, id, value| {
            Ok(Token::new_line(
                arena.clone(),
                id,
                &words(arena.clone(), value)?,
            ))
        },
    )
}

macro_rules! create_paragraph {
    ($arena:ident, $value:expr, $ids:ident) => {
        let id = $arena.write().reserve_id($arena.clone());
        let token = Token::new_paragraph($arena.clone(), id, &lines($arena.clone(), $value)?);
        $arena.write().set(id, token)?;
        link_token($arena.clone(), id, &$ids);
        $ids.push(id);
    };
}

pub fn paragraphs(arena: Arc<RwLock<TokenArena>>, values: &str) -> TokenResult<Vec<TokenId>> {
    let mut ids = Vec::new();
    let mut count = 0;
    let mut from = 0;

    for separator in PARAGRAPH_SEPARATOR.find_iter(values) {
        let until = separator.end();
        create_paragraph!(arena, &values[from..until], ids);
        count += 1;
        from = until;
    }

    if count == 0 {
        create_paragraph!(arena, values, ids);
    } else if from < values.len() {
        create_paragraph!(arena, &values[from..], ids);
    }

    Ok(ids)
}
