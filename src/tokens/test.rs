// Copyright © 2018, 2019 Phil Booth
//
// This file is part of pbvi.
//
// pbvi is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// pbvi is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// pbvi. If not, see <https://www.gnu.org/licenses/>.

use super::*;

#[test]
fn grapheme() {
    let arena = Arc::new(RwLock::new(TokenArena::new()));
    let grapheme_id = arena.write().reserve_id(arena.clone());
    let grapheme = Token::new_grapheme(arena.clone(), 0, " ");
    arena.write().set(grapheme_id, grapheme).unwrap();

    arena.read().read(grapheme_id, &|grapheme| {
        assert_eq!(grapheme.len(), 1);
        assert_eq!(grapheme.to_string(), " ");
        assert_eq!(grapheme.terminates(), TokenType::Grapheme);
        assert!(grapheme.is_grapheme());
        assert!(!grapheme.is_newline_grapheme());
        assert!(grapheme.previous().is_none());
        assert!(grapheme.next().is_none());
    });
}

#[test]
fn unicode_grapheme() {
    let arena = Arc::new(RwLock::new(TokenArena::new()));
    let grapheme_id = arena.write().reserve_id(arena.clone());
    let grapheme = Token::new_grapheme(arena.clone(), 0, "💩");
    arena.write().set(grapheme_id, grapheme).unwrap();

    arena.read().read(grapheme_id, &|grapheme| {
        assert_eq!(grapheme.len(), 1);
        assert_eq!(grapheme.to_string(), "💩");
        assert_eq!(grapheme.terminates(), TokenType::Grapheme);
        assert!(grapheme.is_grapheme());
        assert!(!grapheme.is_newline_grapheme());
        assert!(grapheme.previous().is_none());
        assert!(grapheme.next().is_none());
    });
}

#[test]
fn parse_graphemes() {
    let arena = Arc::new(RwLock::new(TokenArena::new()));
    let expected_graphemes = vec![
        "A", "À", "Ǡ", "\n", "\r\n", "\r", "\t", "ௐ", "A⃢", "ⅰ⃠", "🦀", "🧙",
    ];

    let grapheme_ids = parse::graphemes(arena.clone(), &expected_graphemes.concat()).unwrap();
    assert_eq!(grapheme_ids.len(), expected_graphemes.len());

    grapheme_ids
        .iter()
        .enumerate()
        .for_each(|(index, grapheme_id)| {
            arena.read().read(*grapheme_id, &|grapheme| {
                assert_eq!(grapheme.to_string(), expected_graphemes[index]);
                assert!(grapheme.is_grapheme());

                if index > 0 {
                    assert!(*grapheme_id > grapheme_ids[index - 1]);
                    assert!(grapheme.previous().is_some());
                    assert_eq!(grapheme.previous().unwrap(), grapheme_ids[index - 1]);
                }

                if index < grapheme_ids.len() - 1 {
                    assert!(grapheme.next().is_some());
                    assert_eq!(grapheme.next().unwrap(), grapheme_ids[index + 1]);
                }

                if index == 3 || index == 4 || index == 5 {
                    assert!(grapheme.is_newline_grapheme());
                } else {
                    assert!(!grapheme.is_newline_grapheme());
                }
            });
        });
}

#[test]
fn word() {
    let arena = Arc::new(RwLock::new(TokenArena::new()));
    let grapheme_ids = parse::graphemes(arena.clone(), "wibble").unwrap();
    let word_id = arena.write().reserve_id(arena.clone());
    let word = Token::new_word(arena.clone(), word_id, &grapheme_ids);
    arena.write().set(word_id, word).unwrap();

    arena.read().read(word_id, &|word| {
        assert_eq!(word.len(), 6);
        assert_eq!(word.to_string(), "wibble");
        assert_eq!(word.terminates(), TokenType::Word);
        assert!(!word.is_grapheme());
        assert!(!word.is_newline_grapheme());
        assert!(word.previous().is_none());
        assert!(word.next().is_none());
    });

    grapheme_ids[0..5].iter().for_each(|grapheme_id| {
        arena.read().read(*grapheme_id, &|grapheme| {
            assert_eq!(grapheme.terminates(), TokenType::Grapheme);
        });
    });

    arena.read().read(grapheme_ids[5], &|grapheme| {
        assert_eq!(grapheme.terminates(), TokenType::Word);
    });
}

#[test]
fn unicode_word() {
    let arena = Arc::new(RwLock::new(TokenArena::new()));
    let grapheme_ids = parse::graphemes(arena.clone(), "🐮🐼🐸").unwrap();
    let word_id = arena.write().reserve_id(arena.clone());
    let word = Token::new_word(arena.clone(), word_id, &grapheme_ids);
    arena.write().set(word_id, word).unwrap();

    arena.read().read(word_id, &|word| {
        assert_eq!(word.len(), 3);
        assert_eq!(word.to_string(), "🐮🐼🐸");
        assert!(!word.is_grapheme());
        assert!(!word.is_newline_grapheme());
        assert!(word.previous().is_none());
        assert!(word.next().is_none());
    });
}

#[test]
fn parse_words() {
    let arena = Arc::new(RwLock::new(TokenArena::new()));
    let expected_words = vec![
        "foo", " ", " ", "bar", "\t", "baz", "\n", "\r\n", "\t", " ", "qux", "🦀", "🧙",
    ];

    let word_ids = parse::words(arena.clone(), &expected_words.concat()).unwrap();
    assert_eq!(word_ids.len(), expected_words.len());

    word_ids.iter().enumerate().for_each(|(index, word_id)| {
        arena.read().read(*word_id, &|word| {
            assert_eq!(word.to_string(), expected_words[index]);

            if index > 0 {
                assert!(*word_id > word_ids[index - 1]);
                assert!(word.previous().is_some());
                assert_eq!(word.previous().unwrap(), word_ids[index - 1]);
            }

            if index < word_ids.len() - 1 {
                assert!(word.next().is_some());
                assert_eq!(word.next().unwrap(), word_ids[index + 1]);
            }
        });
    });
}

#[test]
fn line() {
    let arena = Arc::new(RwLock::new(TokenArena::new()));
    let word_ids = parse::words(arena.clone(), "foo bar baz qux").unwrap();
    let line_id = arena.write().reserve_id(arena.clone());
    let line = Token::new_line(arena.clone(), line_id, &word_ids);
    arena.write().set(line_id, line).unwrap();

    arena.read().read(line_id, &|line| {
        assert_eq!(line.len(), 15);
        assert_eq!(line.to_string(), "foo bar baz qux");
        assert_eq!(line.terminates(), TokenType::Line);
        assert!(!line.is_grapheme());
        assert!(!line.is_newline_grapheme());
        assert!(line.previous().is_none());
        assert!(line.next().is_none());
    });

    word_ids[0..5].iter().for_each(|word_id| {
        arena.read().read(*word_id, &|word| {
            assert_eq!(word.terminates(), TokenType::Word);

            let grapheme_ids = arena.read().read(word.first_child().unwrap(), &|grapheme| {
                grapheme.iter(word.first_child(), word.last_child())
            });

            grapheme_ids.enumerate().for_each(|(index, grapheme_id)| {
                arena.read().read(grapheme_id, &|grapheme| {
                    if index < word.len() - 1 {
                        assert_eq!(grapheme.terminates(), TokenType::Grapheme);
                    } else {
                        assert_eq!(grapheme.terminates(), TokenType::Word);
                    }
                });
            });
        });
    });

    arena.read().read(word_ids[6], &|word| {
        assert_eq!(word.terminates(), TokenType::Line);

        let grapheme_ids = arena.read().read(word.first_child().unwrap(), &|grapheme| {
            grapheme.iter(word.first_child(), word.last_child())
        });

        grapheme_ids.enumerate().for_each(|(index, grapheme_id)| {
            arena.read().read(grapheme_id, &|grapheme| {
                if index < word.len() - 1 {
                    assert_eq!(grapheme.terminates(), TokenType::Grapheme);
                } else {
                    assert_eq!(grapheme.terminates(), TokenType::Line);
                }
            });
        });
    });
}

#[test]
fn unicode_line() {
    let arena = Arc::new(RwLock::new(TokenArena::new()));
    let word_ids = parse::words(arena.clone(), "🐮🐼🐸 🍺🍷🍸").unwrap();
    let line_id = arena.write().reserve_id(arena.clone());
    let line = Token::new_line(arena.clone(), line_id, &word_ids);
    arena.write().set(line_id, line).unwrap();

    arena.read().read(line_id, &|line| {
        assert_eq!(line.len(), 7);
        assert_eq!(line.to_string(), "🐮🐼🐸 🍺🍷🍸");
        assert!(!line.is_grapheme());
        assert!(!line.is_newline_grapheme());
        assert!(line.previous().is_none());
        assert!(line.next().is_none());
    });
}

#[test]
fn parse_lines() {
    let arena = Arc::new(RwLock::new(TokenArena::new()));
    let expected_lines = vec!["foo bar\n", "baz\tqux\r\n", "\n", "wibble 🦀🧙"];

    let line_ids = parse::lines(arena.clone(), &expected_lines.concat()).unwrap();
    assert_eq!(line_ids.len(), expected_lines.len());

    line_ids.iter().enumerate().for_each(|(index, line_id)| {
        arena.read().read(*line_id, &|line| {
            assert_eq!(line.to_string(), expected_lines[index]);

            if index > 0 {
                assert!(*line_id > line_ids[index - 1]);
                assert!(line.previous().is_some());
                assert_eq!(line.previous().unwrap(), line_ids[index - 1]);
            }

            if index < line_ids.len() - 1 {
                assert!(line.next().is_some());
                assert_eq!(line.next().unwrap(), line_ids[index + 1]);
            }
        });
    });
}

#[test]
fn paragraph() {
    let arena = Arc::new(RwLock::new(TokenArena::new()));
    let line_ids = parse::lines(arena.clone(), "foo bar\nbaz qux\r\nwibble").unwrap();
    let paragraph_id = arena.write().reserve_id(arena.clone());
    let paragraph = Token::new_paragraph(arena.clone(), paragraph_id, &line_ids);
    arena.write().set(paragraph_id, paragraph).unwrap();

    arena.read().read(paragraph_id, &|paragraph| {
        assert_eq!(paragraph.len(), 22);
        assert_eq!(paragraph.to_string(), "foo bar\nbaz qux\r\nwibble");
        assert_eq!(paragraph.terminates(), TokenType::Paragraph);
        assert!(!paragraph.is_grapheme());
        assert!(!paragraph.is_newline_grapheme());
        assert!(paragraph.previous().is_none());
        assert!(paragraph.next().is_none());
    });

    line_ids[0..2].iter().for_each(|line_id| {
        arena.read().read(*line_id, &|line| {
            assert_eq!(line.terminates(), TokenType::Line);
        });
    });

    arena.read().read(line_ids[2], &|line| {
        assert_eq!(line.terminates(), TokenType::Paragraph);
    });
}

#[test]
fn parse_paragraphs() {
    let arena = Arc::new(RwLock::new(TokenArena::new()));
    let expected_paragraphs = vec!["foo\nbar\n\n", "baz\r\nqux\r\n\r\n\r\n", "wibble 🦀🧙"];

    let paragraph_ids = parse::paragraphs(arena.clone(), &expected_paragraphs.concat()).unwrap();
    assert_eq!(paragraph_ids.len(), expected_paragraphs.len());

    paragraph_ids
        .iter()
        .enumerate()
        .for_each(|(index, paragraph_id)| {
            arena.read().read(*paragraph_id, &|paragraph| {
                assert_eq!(paragraph.to_string(), expected_paragraphs[index]);

                if index > 0 {
                    assert!(*paragraph_id > paragraph_ids[index - 1]);
                    assert!(paragraph.previous().is_some());
                    assert_eq!(paragraph.previous().unwrap(), paragraph_ids[index - 1]);
                }

                if index < paragraph_ids.len() - 1 {
                    assert!(paragraph.next().is_some());
                    assert_eq!(paragraph.next().unwrap(), paragraph_ids[index + 1]);
                }
            });
        });
}

#[test]
fn terminates() {
    let arena = Arc::new(RwLock::new(TokenArena::new()));
    let grapheme_id = arena.write().reserve_id(arena.clone());
    let grapheme = Token::new_grapheme(arena.clone(), 0, " ");
    arena.write().set(grapheme_id, grapheme).unwrap();

    arena.read().mutate(grapheme_id, &|mut grapheme| {
        grapheme.set_terminates(TokenType::Word);
    });

    arena.read().read(grapheme_id, &|grapheme| {
        assert_eq!(grapheme.len(), 1);
        assert_eq!(grapheme.to_string(), " ");
        assert_eq!(grapheme.terminates(), TokenType::Word);
        assert!(grapheme.previous().is_none());
        assert!(grapheme.next().is_none());
    });

    arena.read().mutate(grapheme_id, &|mut grapheme| {
        grapheme.set_terminates(TokenType::Line);
    });

    arena.read().read(grapheme_id, &|grapheme| {
        assert_eq!(grapheme.terminates(), TokenType::Line);
    });

    arena.read().mutate(grapheme_id, &|mut grapheme| {
        grapheme.set_terminates(TokenType::Paragraph);
    });

    arena.read().read(grapheme_id, &|grapheme| {
        assert_eq!(grapheme.terminates(), TokenType::Paragraph);
    });

    arena.read().mutate(grapheme_id, &|mut grapheme| {
        grapheme.clear_terminates();
    });

    arena.read().read(grapheme_id, &|grapheme| {
        assert_eq!(grapheme.terminates(), TokenType::Grapheme);
    });
}

#[test]
fn previous() {
    let arena = Arc::new(RwLock::new(TokenArena::new()));

    let one_id = arena.write().reserve_id(arena.clone());
    let one = Token::new_grapheme(arena.clone(), one_id, "1");
    arena.write().set(one_id, one).unwrap();

    let two_id = arena.write().reserve_id(arena.clone());
    let two = Token::new_grapheme(arena.clone(), two_id, "2");
    arena.write().set(two_id, two).unwrap();

    assert_ne!(one_id, two_id);

    arena.read().mutate(two_id, &|mut two| {
        two.set_previous(one_id);
    });

    arena.read().read(one_id, &|one| {
        assert!(one.next().is_none());
    });

    arena.read().read(two_id, &|two| {
        assert_eq!(two.len(), 1);
        assert_eq!(two.to_string(), "2");
        assert_eq!(two.terminates(), TokenType::Grapheme);
        assert!(two.previous().is_some());
        assert!(two.next().is_none());
        let previous_id = two.previous().unwrap();
        assert_eq!(previous_id, one_id);
    });

    let zero_id = arena.write().reserve_id(arena.clone());
    let zero = Token::new_grapheme(arena.clone(), zero_id, "0");
    arena.write().set(zero_id, zero).unwrap();

    arena.read().mutate(two_id, &|mut two| {
        two.set_previous_and_cascade(zero_id);
    });

    arena.read().read(zero_id, &|zero| {
        let next_id = zero.next().unwrap();
        assert_eq!(next_id, two_id);
    });

    arena.read().read(two_id, &|two| {
        assert!(two.previous().is_some());
        let previous_id = two.previous().unwrap();
        assert_eq!(previous_id, zero_id);
    });

    arena.read().mutate(two_id, &|mut two| {
        two.clear_previous();
    });

    arena.read().read(two_id, &|two| {
        assert!(two.previous().is_none());
    });
}

#[test]
fn previous_with_children() {
    let arena = Arc::new(RwLock::new(TokenArena::new()));
    let grapheme_ids = parse::graphemes(arena.clone(), "onetwo").unwrap();

    let one_id = arena.write().reserve_id(arena.clone());
    let one = Token::new_word(arena.clone(), one_id, &grapheme_ids[0..3]);
    arena.write().set(one_id, one).unwrap();

    let two_id = arena.write().reserve_id(arena.clone());
    let two = Token::new_word(arena.clone(), two_id, &grapheme_ids[3..]);
    arena.write().set(two_id, two).unwrap();

    arena.read().mutate(two_id, &|mut two| {
        two.set_previous(one_id);
    });

    arena.read().read(one_id, &|one| {
        assert!(one.next().is_none());
    });

    arena.read().read(two_id, &|two| {
        assert!(two.previous().is_some());
        assert!(two.next().is_none());
        let previous_id = two.previous().unwrap();
        assert_eq!(previous_id, one_id);
    });

    let new_grapheme_ids = parse::graphemes(arena.clone(), "zero").unwrap();
    let zero_id = arena.write().reserve_id(arena.clone());
    let zero = Token::new_word(arena.clone(), zero_id, &new_grapheme_ids);
    arena.write().set(zero_id, zero).unwrap();

    arena.read().mutate(two_id, &|mut two| {
        two.set_previous_and_cascade(zero_id);
    });

    arena.read().read(zero_id, &|zero| {
        let next_id = zero.next().unwrap();
        assert_eq!(next_id, two_id);
    });

    arena.read().read(two_id, &|two| {
        assert!(two.previous().is_some());
        let previous_id = two.previous().unwrap();
        assert_eq!(previous_id, zero_id);
    });

    arena.read().read(grapheme_ids[3], &|grapheme| {
        let previous_id = grapheme.previous().unwrap();
        assert_eq!(previous_id, new_grapheme_ids[3]);
    });

    arena.read().read(new_grapheme_ids[3], &|new_grapheme| {
        let next_id = new_grapheme.next().unwrap();
        assert_eq!(next_id, grapheme_ids[3]);
    });

    arena.read().mutate(two_id, &|mut two| {
        two.clear_previous();
    });

    arena.read().read(two_id, &|two| {
        assert!(two.previous().is_none());
    });
}

#[test]
fn next() {
    let arena = Arc::new(RwLock::new(TokenArena::new()));

    let one_id = arena.write().reserve_id(arena.clone());
    let one = Token::new_grapheme(arena.clone(), one_id, "1");
    arena.write().set(one_id, one).unwrap();

    let two_id = arena.write().reserve_id(arena.clone());
    let two = Token::new_grapheme(arena.clone(), two_id, "2");
    arena.write().set(two_id, two).unwrap();

    assert_ne!(one_id, two_id);

    arena.read().mutate(one_id, &|mut one| {
        one.set_next(two_id);
    });

    arena.read().read(one_id, &|one| {
        assert_eq!(one.len(), 1);
        assert_eq!(one.to_string(), "1");
        assert_eq!(one.terminates(), TokenType::Grapheme);
        assert!(one.previous().is_none());
        assert!(one.next().is_some());
        let next_id = one.next().unwrap();
        assert_eq!(next_id, two_id);
    });

    arena.read().read(two_id, &|two| {
        assert!(two.previous().is_none());
    });

    let three_id = arena.write().reserve_id(arena.clone());
    let three = Token::new_grapheme(arena.clone(), three_id, "3");
    arena.write().set(three_id, three).unwrap();

    arena.read().mutate(one_id, &|mut one| {
        one.set_next_and_cascade(three_id);
    });

    arena.read().read(one_id, &|one| {
        assert!(one.next().is_some());
        let next_id = one.next().unwrap();
        assert_eq!(next_id, three_id);
    });

    arena.read().read(three_id, &|three| {
        let previous_id = three.previous().unwrap();
        assert_eq!(previous_id, one_id);
    });

    arena.read().mutate(one_id, &|mut one| {
        one.clear_next();
    });

    arena.read().read(one_id, &|one| {
        assert!(one.next().is_none());
    });
}

#[test]
fn next_with_children() {
    let arena = Arc::new(RwLock::new(TokenArena::new()));
    let grapheme_ids = parse::graphemes(arena.clone(), "onetwo").unwrap();

    let one_id = arena.write().reserve_id(arena.clone());
    let one = Token::new_word(arena.clone(), one_id, &grapheme_ids[0..3]);
    arena.write().set(one_id, one).unwrap();

    let two_id = arena.write().reserve_id(arena.clone());
    let two = Token::new_word(arena.clone(), two_id, &grapheme_ids[3..]);
    arena.write().set(two_id, two).unwrap();

    assert_ne!(one_id, two_id);

    arena.read().mutate(one_id, &|mut one| {
        one.set_next(two_id);
    });

    arena.read().read(one_id, &|one| {
        assert!(one.previous().is_none());
        assert!(one.next().is_some());
        let next_id = one.next().unwrap();
        assert_eq!(next_id, two_id);
    });

    arena.read().read(two_id, &|two| {
        assert!(two.previous().is_none());
    });

    let new_grapheme_ids = parse::graphemes(arena.clone(), "three").unwrap();
    let three_id = arena.write().reserve_id(arena.clone());
    let three = Token::new_word(arena.clone(), three_id, &new_grapheme_ids);
    arena.write().set(three_id, three).unwrap();

    arena.read().mutate(one_id, &|mut one| {
        one.set_next_and_cascade(three_id);
    });

    arena.read().read(one_id, &|one| {
        assert!(one.next().is_some());
        let next_id = one.next().unwrap();
        assert_eq!(next_id, three_id);
    });

    arena.read().read(three_id, &|three| {
        let previous_id = three.previous().unwrap();
        assert_eq!(previous_id, one_id);
    });

    arena.read().read(grapheme_ids[2], &|grapheme| {
        let next_id = grapheme.next().unwrap();
        assert_eq!(next_id, new_grapheme_ids[0]);
    });

    arena.read().read(new_grapheme_ids[0], &|new_grapheme| {
        let previous_id = new_grapheme.previous().unwrap();
        assert_eq!(previous_id, grapheme_ids[2]);
    });

    arena.read().mutate(one_id, &|mut one| {
        one.clear_next();
    });

    arena.read().read(one_id, &|one| {
        assert!(one.next().is_none());
    });
}

#[test]
fn delete_middle_token() {
    let arena = Arc::new(RwLock::new(TokenArena::new()));
    let grapheme_ids = parse::graphemes(arena.clone(), "123").unwrap();

    arena.read().mutate(grapheme_ids[1], &|mut two| {
        two.delete();
    });

    arena.read().read(grapheme_ids[0], &|one| {
        let next_id = one.next().unwrap();
        assert_eq!(next_id, grapheme_ids[2]);
    });

    arena.read().read(grapheme_ids[1], &|two| {
        let previous_id = two.previous().unwrap();
        assert_eq!(previous_id, grapheme_ids[0]);
        let next_id = two.next().unwrap();
        assert_eq!(next_id, grapheme_ids[2]);
    });

    arena.read().read(grapheme_ids[2], &|three| {
        let previous_id = three.previous().unwrap();
        assert_eq!(previous_id, grapheme_ids[0]);
    });

    arena.read().mutate(grapheme_ids[1], &|mut two| {
        two.reinstate();
    });

    arena.read().read(grapheme_ids[0], &|one| {
        let next_id = one.next().unwrap();
        assert_eq!(next_id, grapheme_ids[1]);
    });

    arena.read().read(grapheme_ids[1], &|two| {
        let previous_id = two.previous().unwrap();
        assert_eq!(previous_id, grapheme_ids[0]);
        let next_id = two.next().unwrap();
        assert_eq!(next_id, grapheme_ids[2]);
    });

    arena.read().read(grapheme_ids[2], &|three| {
        let previous_id = three.previous().unwrap();
        assert_eq!(previous_id, grapheme_ids[1]);
    });
}

#[test]
fn delete_middle_token_with_children() {
    let arena = Arc::new(RwLock::new(TokenArena::new()));
    let grapheme_ids = parse::graphemes(arena.clone(), "onetwothree").unwrap();

    let one_id = arena.write().reserve_id(arena.clone());
    let one = Token::new_word(arena.clone(), one_id, &grapheme_ids[0..3]);
    arena.write().set(one_id, one).unwrap();

    let two_id = arena.write().reserve_id(arena.clone());
    let two = Token::new_word(arena.clone(), two_id, &grapheme_ids[3..6]);
    arena.write().set(two_id, two).unwrap();

    let three_id = arena.write().reserve_id(arena.clone());
    let three = Token::new_word(arena.clone(), three_id, &grapheme_ids[6..]);
    arena.write().set(three_id, three).unwrap();

    arena.read().mutate(one_id, &|mut one| {
        one.set_next_and_cascade(two_id);
    });

    arena.read().mutate(two_id, &|mut two| {
        two.set_next_and_cascade(three_id);
    });

    arena.read().read(grapheme_ids[2], &|grapheme| {
        let next_id = grapheme.next().unwrap();
        assert_eq!(next_id, grapheme_ids[3]);
    });

    arena.read().read(grapheme_ids[3], &|grapheme| {
        let previous_id = grapheme.previous().unwrap();
        assert_eq!(previous_id, grapheme_ids[2]);
    });

    arena.read().read(grapheme_ids[5], &|grapheme| {
        let next_id = grapheme.next().unwrap();
        assert_eq!(next_id, grapheme_ids[6]);
    });

    arena.read().read(grapheme_ids[6], &|grapheme| {
        let previous_id = grapheme.previous().unwrap();
        assert_eq!(previous_id, grapheme_ids[5]);
    });

    arena.read().mutate(two_id, &|mut two| {
        two.delete();
    });

    arena.read().read(one_id, &|one| {
        let next_id = one.next().unwrap();
        assert_eq!(next_id, three_id);
    });

    arena.read().read(two_id, &|two| {
        let previous_id = two.previous().unwrap();
        assert_eq!(previous_id, one_id);
        let next_id = two.next().unwrap();
        assert_eq!(next_id, three_id);
    });

    arena.read().read(three_id, &|three| {
        let previous_id = three.previous().unwrap();
        assert_eq!(previous_id, one_id);
    });

    arena.read().read(grapheme_ids[2], &|grapheme| {
        let next_id = grapheme.next().unwrap();
        assert_eq!(next_id, grapheme_ids[6]);
    });

    arena.read().read(grapheme_ids[6], &|grapheme| {
        let previous_id = grapheme.previous().unwrap();
        assert_eq!(previous_id, grapheme_ids[2]);
    });

    arena.read().mutate(two_id, &|mut two| {
        two.reinstate();
    });

    arena.read().read(one_id, &|one| {
        let next_id = one.next().unwrap();
        assert_eq!(next_id, two_id);
    });

    arena.read().read(two_id, &|two| {
        let previous_id = two.previous().unwrap();
        assert_eq!(previous_id, one_id);
        let next_id = two.next().unwrap();
        assert_eq!(next_id, three_id);
    });

    arena.read().read(three_id, &|three| {
        let previous_id = three.previous().unwrap();
        assert_eq!(previous_id, two_id);
    });

    arena.read().read(grapheme_ids[2], &|grapheme| {
        let next_id = grapheme.next().unwrap();
        assert_eq!(next_id, grapheme_ids[3]);
    });

    arena.read().read(grapheme_ids[6], &|grapheme| {
        let previous_id = grapheme.previous().unwrap();
        assert_eq!(previous_id, grapheme_ids[5]);
    });
}

#[test]
fn delete_middle_token_with_parent() {
    let arena = Arc::new(RwLock::new(TokenArena::new()));
    let grapheme_ids = parse::graphemes(arena.clone(), "123").unwrap();
    let word_id = arena.write().reserve_id(arena.clone());
    let word = Token::new_word(arena.clone(), word_id, &grapheme_ids);
    arena.write().set(word_id, word).unwrap();

    arena
        .read()
        .mutate(grapheme_ids[1], &|mut middle_grapheme| {
            middle_grapheme.delete();
        });

    arena.read().read(word_id, &|word| {
        assert_eq!(word.len(), 2);
        assert_eq!(word.to_string(), "13");
    });

    arena
        .read()
        .mutate(grapheme_ids[1], &|mut middle_grapheme| {
            middle_grapheme.reinstate();
        });

    arena.read().read(word_id, &|word| {
        assert_eq!(word.len(), 3);
        assert_eq!(word.to_string(), "123");
    });
}

#[test]
fn replace_middle_token() {
    let arena = Arc::new(RwLock::new(TokenArena::new()));
    let grapheme_ids = parse::graphemes(arena.clone(), "123").unwrap();
    let replacement_id = arena.write().reserve_id(arena.clone());
    let replacement = Token::new_grapheme(arena.clone(), replacement_id, "b");
    arena.write().set(replacement_id, replacement).unwrap();

    arena.read().mutate(grapheme_ids[1], &|mut two| {
        two.replace(replacement_id);
    });

    arena.read().read(grapheme_ids[0], &|one| {
        let next_id = one.next().unwrap();
        assert_eq!(next_id, replacement_id);
    });

    arena.read().read(grapheme_ids[1], &|two| {
        let previous_id = two.previous().unwrap();
        assert_eq!(previous_id, grapheme_ids[0]);
        let next_id = two.next().unwrap();
        assert_eq!(next_id, grapheme_ids[2]);
    });

    arena.read().read(grapheme_ids[2], &|three| {
        let previous_id = three.previous().unwrap();
        assert_eq!(previous_id, replacement_id);
    });

    arena.read().read(replacement_id, &|replacement| {
        let previous_id = replacement.previous().unwrap();
        assert_eq!(previous_id, grapheme_ids[0]);
        let next_id = replacement.next().unwrap();
        assert_eq!(next_id, grapheme_ids[2]);
    });
}

#[test]
fn replace_middle_token_with_children() {
    let arena = Arc::new(RwLock::new(TokenArena::new()));
    let grapheme_ids = parse::graphemes(arena.clone(), "onetwothree").unwrap();

    let one_id = arena.write().reserve_id(arena.clone());
    let one = Token::new_word(arena.clone(), one_id, &grapheme_ids[0..3]);
    arena.write().set(one_id, one).unwrap();

    let two_id = arena.write().reserve_id(arena.clone());
    let two = Token::new_word(arena.clone(), two_id, &grapheme_ids[3..6]);
    arena.write().set(two_id, two).unwrap();

    let three_id = arena.write().reserve_id(arena.clone());
    let three = Token::new_word(arena.clone(), three_id, &grapheme_ids[6..]);
    arena.write().set(three_id, three).unwrap();

    let replacement_grapheme_ids = parse::graphemes(arena.clone(), "alt").unwrap();
    let replacement_id = arena.write().reserve_id(arena.clone());
    let replacement = Token::new_word(arena.clone(), replacement_id, &replacement_grapheme_ids);
    arena.write().set(replacement_id, replacement).unwrap();

    arena.read().mutate(one_id, &|mut one| {
        one.set_next_and_cascade(two_id);
    });

    arena.read().mutate(two_id, &|mut two| {
        two.set_next_and_cascade(three_id);
        two.replace(replacement_id);
    });

    arena.read().read(one_id, &|one| {
        let next_id = one.next().unwrap();
        assert_eq!(next_id, replacement_id);
    });

    arena.read().read(two_id, &|two| {
        let previous_id = two.previous().unwrap();
        assert_eq!(previous_id, one_id);
        let next_id = two.next().unwrap();
        assert_eq!(next_id, three_id);
    });

    arena.read().read(three_id, &|three| {
        let previous_id = three.previous().unwrap();
        assert_eq!(previous_id, replacement_id);
    });

    arena.read().read(replacement_id, &|replacement| {
        let previous_id = replacement.previous().unwrap();
        assert_eq!(previous_id, one_id);
        let next_id = replacement.next().unwrap();
        assert_eq!(next_id, three_id);
    });

    arena.read().read(grapheme_ids[2], &|grapheme| {
        let next_id = grapheme.next().unwrap();
        assert_eq!(next_id, replacement_grapheme_ids[0]);
    });

    arena.read().read(grapheme_ids[6], &|grapheme| {
        let previous_id = grapheme.previous().unwrap();
        assert_eq!(previous_id, replacement_grapheme_ids[2]);
    });

    arena
        .read()
        .read(replacement_grapheme_ids[0], &|replacement_grapheme| {
            let previous_id = replacement_grapheme.previous().unwrap();
            assert_eq!(previous_id, grapheme_ids[2]);
        });

    arena
        .read()
        .read(replacement_grapheme_ids[2], &|replacement_grapheme| {
            let next_id = replacement_grapheme.next().unwrap();
            assert_eq!(next_id, grapheme_ids[6]);
        });
}

#[test]
fn replace_middle_token_with_parent() {
    let arena = Arc::new(RwLock::new(TokenArena::new()));
    let grapheme_ids = parse::graphemes(arena.clone(), "123").unwrap();

    let replacement_id = arena.write().reserve_id(arena.clone());
    let replacement = Token::new_grapheme(arena.clone(), replacement_id, "b");
    arena.write().set(replacement_id, replacement).unwrap();

    let word_id = arena.write().reserve_id(arena.clone());
    let word = Token::new_word(arena.clone(), word_id, &grapheme_ids);
    arena.write().set(word_id, word).unwrap();

    arena
        .read()
        .mutate(grapheme_ids[1], &|mut middle_grapheme| {
            middle_grapheme.replace(replacement_id);
        });

    arena.read().read(word_id, &|word| {
        assert_eq!(word.len(), 3);
        assert_eq!(word.to_string(), "1b3");
    });
}

#[test]
fn delete_first_token() {
    let arena = Arc::new(RwLock::new(TokenArena::new()));
    let grapheme_ids = parse::graphemes(arena.clone(), "12").unwrap();

    arena.read().mutate(grapheme_ids[0], &|mut one| {
        one.delete();
    });

    arena.read().read(grapheme_ids[1], &|two| {
        assert!(two.previous().is_none());
    });

    arena.read().mutate(grapheme_ids[0], &|mut one| {
        one.reinstate();
    });

    arena.read().read(grapheme_ids[1], &|two| {
        let previous_id = two.previous().unwrap();
        assert_eq!(previous_id, grapheme_ids[0]);
    });
}

#[test]
fn delete_first_token_with_children() {
    let arena = Arc::new(RwLock::new(TokenArena::new()));
    let grapheme_ids = parse::graphemes(arena.clone(), "onetwo").unwrap();

    let one_id = arena.write().reserve_id(arena.clone());
    let one = Token::new_word(arena.clone(), one_id, &grapheme_ids[0..3]);
    arena.write().set(one_id, one).unwrap();

    let two_id = arena.write().reserve_id(arena.clone());
    let two = Token::new_word(arena.clone(), two_id, &grapheme_ids[3..]);
    arena.write().set(two_id, two).unwrap();

    arena.read().mutate(one_id, &|mut one| {
        one.set_next_and_cascade(two_id);
        one.delete();
    });

    arena.read().read(two_id, &|two| {
        assert!(two.previous().is_none());
    });

    arena.read().read(grapheme_ids[3], &|grapheme| {
        assert!(grapheme.previous().is_none());
    });

    arena.read().mutate(one_id, &|mut one| {
        one.reinstate();
    });

    arena.read().read(two_id, &|two| {
        let previous_id = two.previous().unwrap();
        assert_eq!(previous_id, one_id);
    });

    arena.read().read(grapheme_ids[3], &|grapheme| {
        let previous_id = grapheme.previous().unwrap();
        assert_eq!(previous_id, grapheme_ids[2]);
    });
}

#[test]
fn delete_first_token_with_parent() {
    let arena = Arc::new(RwLock::new(TokenArena::new()));
    let grapheme_ids = parse::graphemes(arena.clone(), "12").unwrap();
    let word_id = arena.write().reserve_id(arena.clone());
    let word = Token::new_word(arena.clone(), word_id, &grapheme_ids);
    arena.write().set(word_id, word).unwrap();

    arena.read().mutate(grapheme_ids[0], &|mut first_grapheme| {
        first_grapheme.delete();
    });

    arena.read().read(word_id, &|word| {
        assert_eq!(word.len(), 1);
        assert_eq!(word.to_string(), "2");
    });

    arena.read().mutate(grapheme_ids[0], &|mut first_grapheme| {
        first_grapheme.reinstate();
    });

    arena.read().read(word_id, &|word| {
        assert_eq!(word.len(), 2);
        assert_eq!(word.to_string(), "12");
    });
}

#[test]
fn replace_first_token() {
    let arena = Arc::new(RwLock::new(TokenArena::new()));
    let grapheme_ids = parse::graphemes(arena.clone(), "12").unwrap();
    let replacement_id = arena.write().reserve_id(arena.clone());
    let replacement = Token::new_grapheme(arena.clone(), replacement_id, "a");
    arena.write().set(replacement_id, replacement).unwrap();

    arena.read().mutate(grapheme_ids[0], &|mut one| {
        one.replace(replacement_id);
    });

    arena.read().read(grapheme_ids[1], &|two| {
        let previous_id = two.previous().unwrap();
        assert_eq!(previous_id, replacement_id);
    });

    arena.read().read(replacement_id, &|replacement| {
        assert!(replacement.previous().is_none());
        let next_id = replacement.next().unwrap();
        assert_eq!(next_id, grapheme_ids[1]);
    });
}

#[test]
fn replace_first_token_with_children() {
    let arena = Arc::new(RwLock::new(TokenArena::new()));
    let grapheme_ids = parse::graphemes(arena.clone(), "onetwothree").unwrap();

    let one_id = arena.write().reserve_id(arena.clone());
    let one = Token::new_word(arena.clone(), one_id, &grapheme_ids[0..3]);
    arena.write().set(one_id, one).unwrap();

    let two_id = arena.write().reserve_id(arena.clone());
    let two = Token::new_word(arena.clone(), two_id, &grapheme_ids[3..]);
    arena.write().set(two_id, two).unwrap();

    let replacement_grapheme_ids = parse::graphemes(arena.clone(), "alt").unwrap();
    let replacement_id = arena.write().reserve_id(arena.clone());
    let replacement = Token::new_word(arena.clone(), replacement_id, &replacement_grapheme_ids);
    arena.write().set(replacement_id, replacement).unwrap();

    arena.read().mutate(one_id, &|mut one| {
        one.set_next_and_cascade(two_id);
        one.replace(replacement_id);
    });

    arena.read().read(two_id, &|two| {
        let previous_id = two.previous().unwrap();
        assert_eq!(previous_id, replacement_id);
    });

    arena.read().read(replacement_id, &|replacement| {
        assert!(replacement.previous().is_none());
        let next_id = replacement.next().unwrap();
        assert_eq!(next_id, two_id);
    });

    arena
        .read()
        .read(replacement_grapheme_ids[2], &|replacement_grapheme| {
            let next_id = replacement_grapheme.next().unwrap();
            assert_eq!(next_id, grapheme_ids[3]);
        });

    arena.read().read(grapheme_ids[3], &|grapheme| {
        let previous_id = grapheme.previous().unwrap();
        assert_eq!(previous_id, replacement_grapheme_ids[2]);
    });
}

#[test]
fn replace_first_token_with_parent() {
    let arena = Arc::new(RwLock::new(TokenArena::new()));
    let grapheme_ids = parse::graphemes(arena.clone(), "12").unwrap();

    let replacement_id = arena.write().reserve_id(arena.clone());
    let replacement = Token::new_grapheme(arena.clone(), replacement_id, "a");
    arena.write().set(replacement_id, replacement).unwrap();

    let word_id = arena.write().reserve_id(arena.clone());
    let word = Token::new_word(arena.clone(), word_id, &grapheme_ids);
    arena.write().set(word_id, word).unwrap();

    arena.read().mutate(grapheme_ids[0], &|mut first_grapheme| {
        first_grapheme.replace(replacement_id);
    });

    arena.read().read(word_id, &|word| {
        assert_eq!(word.len(), 2);
        assert_eq!(word.to_string(), "a2");
    });
}

#[test]
fn delete_last_token() {
    let arena = Arc::new(RwLock::new(TokenArena::new()));
    let grapheme_ids = parse::graphemes(arena.clone(), "12").unwrap();

    arena.read().mutate(grapheme_ids[1], &|mut two| {
        two.delete();
    });

    arena.read().read(grapheme_ids[0], &|one| {
        assert!(one.next().is_none());
    });

    arena.read().mutate(grapheme_ids[1], &|mut two| {
        two.reinstate();
    });

    arena.read().read(grapheme_ids[0], &|one| {
        let next_id = one.next().unwrap();
        assert_eq!(next_id, grapheme_ids[1]);
    });
}

#[test]
fn delete_last_token_with_children() {
    let arena = Arc::new(RwLock::new(TokenArena::new()));
    let grapheme_ids = parse::graphemes(arena.clone(), "onetwo").unwrap();

    let one_id = arena.write().reserve_id(arena.clone());
    let one = Token::new_word(arena.clone(), one_id, &grapheme_ids[0..3]);
    arena.write().set(one_id, one).unwrap();

    let two_id = arena.write().reserve_id(arena.clone());
    let two = Token::new_word(arena.clone(), two_id, &grapheme_ids[3..]);
    arena.write().set(two_id, two).unwrap();

    arena.read().mutate(one_id, &|mut one| {
        one.set_next_and_cascade(two_id);
    });

    arena.read().mutate(two_id, &|mut two| {
        two.delete();
    });

    arena.read().read(one_id, &|one| {
        assert!(one.next().is_none());
    });

    arena.read().read(grapheme_ids[2], &|grapheme| {
        assert!(grapheme.next().is_none());
    });

    arena.read().mutate(two_id, &|mut two| {
        two.reinstate();
    });

    arena.read().read(one_id, &|one| {
        let next_id = one.next().unwrap();
        assert_eq!(next_id, two_id);
    });

    arena.read().read(grapheme_ids[2], &|grapheme| {
        let next_id = grapheme.next().unwrap();
        assert_eq!(next_id, grapheme_ids[3]);
    });
}

#[test]
fn delete_last_token_with_parent() {
    let arena = Arc::new(RwLock::new(TokenArena::new()));
    let grapheme_ids = parse::graphemes(arena.clone(), "12").unwrap();

    let word_id = arena.write().reserve_id(arena.clone());
    let word = Token::new_word(arena.clone(), word_id, &grapheme_ids);
    arena.write().set(word_id, word).unwrap();

    arena.read().mutate(grapheme_ids[1], &|mut last_grapheme| {
        last_grapheme.delete();
    });

    arena.read().read(word_id, &|word| {
        assert_eq!(word.len(), 1);
        assert_eq!(word.to_string(), "1");
    });

    arena.read().mutate(grapheme_ids[1], &|mut last_grapheme| {
        last_grapheme.reinstate();
    });

    arena.read().read(word_id, &|word| {
        assert_eq!(word.len(), 2);
        assert_eq!(word.to_string(), "12");
    });
}

#[test]
fn replace_last_token() {
    let arena = Arc::new(RwLock::new(TokenArena::new()));
    let grapheme_ids = parse::graphemes(arena.clone(), "12").unwrap();

    let replacement_id = arena.write().reserve_id(arena.clone());
    let replacement = Token::new_grapheme(arena.clone(), replacement_id, "b");
    arena.write().set(replacement_id, replacement).unwrap();

    arena.read().mutate(grapheme_ids[1], &|mut two| {
        two.replace(replacement_id);
    });

    arena.read().read(grapheme_ids[0], &|one| {
        let next_id = one.next().unwrap();
        assert_eq!(next_id, replacement_id);
    });

    arena.read().read(replacement_id, &|replacement| {
        assert!(replacement.next().is_none());
        let previous_id = replacement.previous().unwrap();
        assert_eq!(previous_id, grapheme_ids[0]);
    });
}

#[test]
fn replace_last_token_with_children() {
    let arena = Arc::new(RwLock::new(TokenArena::new()));
    let grapheme_ids = parse::graphemes(arena.clone(), "onetwothree").unwrap();

    let one_id = arena.write().reserve_id(arena.clone());
    let one = Token::new_word(arena.clone(), one_id, &grapheme_ids[0..3]);
    arena.write().set(one_id, one).unwrap();

    let two_id = arena.write().reserve_id(arena.clone());
    let two = Token::new_word(arena.clone(), two_id, &grapheme_ids[3..]);
    arena.write().set(two_id, two).unwrap();

    let replacement_grapheme_ids = parse::graphemes(arena.clone(), "alt").unwrap();
    let replacement_id = arena.write().reserve_id(arena.clone());
    let replacement = Token::new_word(arena.clone(), replacement_id, &replacement_grapheme_ids);
    arena.write().set(replacement_id, replacement).unwrap();

    arena.read().mutate(one_id, &|mut one| {
        one.set_next_and_cascade(two_id);
    });

    arena.read().mutate(two_id, &|mut two| {
        two.replace(replacement_id);
    });

    arena.read().read(one_id, &|one| {
        let next_id = one.next().unwrap();
        assert_eq!(next_id, replacement_id);
    });

    arena.read().read(replacement_id, &|replacement| {
        assert!(replacement.next().is_none());
        let previous_id = replacement.previous().unwrap();
        assert_eq!(previous_id, one_id);
    });

    arena
        .read()
        .read(replacement_grapheme_ids[0], &|replacement_grapheme| {
            let previous_id = replacement_grapheme.previous().unwrap();
            assert_eq!(previous_id, grapheme_ids[2]);
        });

    arena.read().read(grapheme_ids[2], &|grapheme| {
        let next_id = grapheme.next().unwrap();
        assert_eq!(next_id, replacement_grapheme_ids[0]);
    });
}

#[test]
fn replace_last_token_with_parent() {
    let arena = Arc::new(RwLock::new(TokenArena::new()));
    let grapheme_ids = parse::graphemes(arena.clone(), "12").unwrap();

    let replacement_id = arena.write().reserve_id(arena.clone());
    let replacement = Token::new_grapheme(arena.clone(), replacement_id, "b");
    arena.write().set(replacement_id, replacement).unwrap();

    let word_id = arena.write().reserve_id(arena.clone());
    let word = Token::new_word(arena.clone(), word_id, &grapheme_ids);
    arena.write().set(word_id, word).unwrap();

    arena.read().mutate(grapheme_ids[1], &|mut last_grapheme| {
        last_grapheme.replace(replacement_id);
    });

    arena.read().read(word_id, &|word| {
        assert_eq!(word.len(), 2);
        assert_eq!(word.to_string(), "1b");
    });
}
