// Copyright © 2018, 2019 Phil Booth
//
// This file is part of pbvi.
//
// pbvi is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// pbvi is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// pbvi. If not, see <https://www.gnu.org/licenses/>.

use std::{
    error::Error,
    fmt::{self, Display, Formatter},
};

pub type TokenResult<T> = Result<T, TokenError>;

#[derive(Debug)]
pub struct TokenError(String);

impl TokenError {
    pub fn invalid_id(method: &str) -> TokenError {
        TokenError(format!("invalid id passed to Token::{}", method))
    }

    pub fn poisoned_lock() -> TokenError {
        TokenError(String::from("poisoned lock"))
    }

    pub fn failed_read() -> TokenError {
        TokenError(String::from("failed to acquire shared read lock"))
    }

    pub fn failed_write() -> TokenError {
        TokenError(String::from("failed to acquire write lock"))
    }
}

impl Display for TokenError {
    fn fmt(&self, formatter: &mut Formatter) -> fmt::Result {
        write!(formatter, "{}", &self.0)
    }
}

impl Error for TokenError {}
