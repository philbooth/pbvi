// Copyright © 2018, 2019 Phil Booth
//
// This file is part of pbvi.
//
// pbvi is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later
// version.
//
// pbvi is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
// details.
//
// You should have received a copy of the GNU General Public License along with
// pbvi. If not, see <https://www.gnu.org/licenses/>.

use std::sync::Arc;

use parking_lot::{RwLock, RwLockReadGuard};

use super::{TokenArena, TokenId};

#[derive(Debug)]
pub struct TokenIterator {
    arena: Arc<RwLock<TokenArena>>,
    current: Option<TokenId>,
    first: Option<TokenId>,
    last: Option<TokenId>,
}

impl TokenIterator {
    pub fn new(
        arena: Arc<RwLock<TokenArena>>,
        current: Option<TokenId>,
        first: Option<TokenId>,
        last: Option<TokenId>,
    ) -> Self {
        Self {
            arena,
            current,
            first,
            last,
        }
    }

    pub fn null(arena: Arc<RwLock<TokenArena>>) -> Self {
        Self {
            arena,
            current: None,
            first: None,
            last: None,
        }
    }

    fn arena(&self) -> RwLockReadGuard<TokenArena> {
        self.arena.read()
    }
}

impl Iterator for TokenIterator {
    type Item = TokenId;

    fn next(&mut self) -> Option<Self::Item> {
        let result = self.current;

        self.current = self.current.and_then(|current_id| {
            if let Some(last_id) = self.last {
                if last_id == current_id {
                    return None;
                }
            }

            self.arena().read(current_id, &|current| current.next())
        });

        result
    }
}

impl DoubleEndedIterator for TokenIterator {
    fn next_back(&mut self) -> Option<Self::Item> {
        let result = self.current;

        self.current = self.current.and_then(|current_id| {
            if let Some(first_id) = self.first {
                if first_id == current_id {
                    return None;
                }
            }

            self.arena().read(current_id, &|current| current.previous())
        });

        result
    }
}
